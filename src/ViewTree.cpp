#include "ViewTree.h"
#include "ui_ViewTree.h"
#include "Project.h"

#include "GlobalData.h"
#include "ModelTest.h"

#include <QMenu>
#include <QAction>
#include <typeinfo>
#include <QSortFilterProxyModel>

namespace ProjectContainer
{
///////////////////////////////////////////////////////////////////////////////
// SortProxy

class SortProxy : public QSortFilterProxyModel
{
public:
    SortProxy(QObject *parent)
        : QSortFilterProxyModel(parent)
    {
    }

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
    {
        QModelIndex idx = sourceModel()->index(source_row, 0, source_parent);
        QString data = sourceModel()->data(idx).toString();
        QRegExp rx = filterRegExp();

        if (int rows = sourceModel()->rowCount(idx)) {
            for (int x=0; x<rows; x++) {
                if (filterAcceptsRow(x, idx)) {
                    return true;
                }
            }
        }

        return data.contains(rx);
    }
};

///////////////////////////////////////////////////////////////////////////////
// TreeView

TreeView::TreeView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProjectTree)
{
    ui->setupUi(this);

    m_proxy = new SortProxy(ui->treeView);
    m_proxy->setSourceModel(&Global::ResourceProjects->Content);
    m_proxy->setDynamicSortFilter(true);
    ui->treeView->setModel(m_proxy);
    m_proxy->sort(0, Qt::AscendingOrder);

    //new ModelTest(&Global::Data.ResourceProjects.TreeData, this);

    connect(ui->treeView, SIGNAL(customContextMenuRequested(const QPoint&)),
            SLOT(onTreeContextMenu(const QPoint&)));

    createActions();
}

TreeView::~TreeView()
{
    delete ui;
}

void TreeView::createActions()
{
    m_actionDeleteProject = new QAction(Global::Icons->get(Global::Icons->PackageDelete),
                                        tr("Delete Project"), this);
    connect(m_actionDeleteProject, SIGNAL(triggered()), SLOT(onActionDeleteProject()));

    m_actionCreateTable   = new QAction(Global::Icons->get(Global::Icons->TableAdd),
                                        tr("New String Table"), this);
    connect(m_actionCreateTable, SIGNAL(triggered()), SLOT(onActionCreateTable()));

    m_actionDeleteTable   = new QAction(Global::Icons->get(Global::Icons->TableDelete),
                                        tr("Delete String Table"), this);
    connect(m_actionDeleteTable, SIGNAL(triggered()), SLOT(onActionDeleteTable()));

    m_actionCreateEntry   = new QAction(Global::Icons->get(Global::Icons->PencilAdd),
                                        tr("New String"), this);
    connect(m_actionCreateEntry, SIGNAL(triggered()), SLOT(onActionCreateEntry()));

    m_actionDeleteEntry   = new QAction(Global::Icons->get(Global::Icons->PencilDelete),
                                        tr("Delete String"), this);
    connect(m_actionDeleteEntry, SIGNAL(triggered()), SLOT(onActionDeleteEntry()));
}

void TreeView::onTreeContextMenu(const QPoint &pos)
{
    QModelIndex idx = m_proxy->mapToSource(ui->treeView->indexAt(pos));
    //QModelIndex idx = ui->treeView->indexAt(pos);
    void *ptr = idx.internalPointer();
    QObject *itf = reinterpret_cast<QObject*>(ptr);
    if (!ptr) return;

    QMenu menu;
    QList<QAction*> actions;
    if (typeid(*itf) == typeid(ResourceProject::Data)) {
        Global::Actions->ConfigureProject->setData(QVariant::fromValue(itf));
        m_actionCreateTable->setData(QVariant::fromValue(itf));
        m_actionDeleteProject->setData(QVariant::fromValue(itf));

        actions.push_back(Global::Actions->ConfigureProject);
        actions.push_back(m_actionCreateTable);
        actions.push_back(m_actionDeleteProject);
    } else if (typeid(*itf) == typeid(StringTable::Data)) {
        Global::Actions->ConfigureTable->setData(QVariant::fromValue(itf));
        m_actionCreateEntry->setData(QVariant::fromValue(itf));
        m_actionDeleteTable->setData(QVariant::fromValue(itf));

        actions.push_back(Global::Actions->ConfigureTable);
        actions.push_back(m_actionCreateEntry);
        actions.push_back(m_actionDeleteTable);
    } else if (typeid(*itf) == typeid(StringTable::Entry)) {
        m_actionDeleteEntry->setData(QVariant::fromValue(itf));

        actions.push_back(m_actionDeleteEntry);
    }

    menu.addActions(actions);

    if (menu.actions().size()) {
        menu.exec(ui->treeView->mapToGlobal(pos));
    }
}

void TreeView::onActionDeleteProject()
{
    QAction *act = dynamic_cast<QAction*>(sender());
    if (!act) return;
    ResourceProject::Data *data = dynamic_cast<ResourceProject::Data*>(act->data().value<QObject*>());
    if (data) {
        Global::ResourceProjects->deleteProject(data->id());
    }
}

void TreeView::onActionCreateTable()
{
    QAction *act = dynamic_cast<QAction*>(sender());
    if (!act) return;
    ResourceProject::Data *data = dynamic_cast<ResourceProject::Data*>(act->data().value<QObject*>());
    if (data) {
        data->createStringTable(QString());
    }
}

void TreeView::onActionDeleteTable()
{
    QAction *act = dynamic_cast<QAction*>(sender());
    if (!act) return;
    StringTable::Data *data = dynamic_cast<StringTable::Data*>(act->data().value<QObject*>());
    if (data) {
        if (ResourceProject::Data *parent = dynamic_cast<ResourceProject::Data*>(data->parent())) {
            parent->deleteStringTable(data);
        }
    }
}

void TreeView::onActionCreateEntry()
{
    QAction *act = dynamic_cast<QAction*>(sender());
    if (!act) return;
    StringTable::Data *data = dynamic_cast<StringTable::Data*>(act->data().value<QObject*>());
    if (data) {
        data->createEntry(QString());
    }
}

void TreeView::onActionDeleteEntry()
{
    QAction *act = dynamic_cast<QAction*>(sender());
    if (!act) return;
    StringTable::Entry *data = dynamic_cast<StringTable::Entry*>(act->data().value<QObject*>());
    if (data) {
        if (StringTable::Data *parent = dynamic_cast<StringTable::Data*>(data->parent())) {
            parent->deleteEntry(data);
        }
    }
}

void TreeView::on_lineEdit_textChanged(const QString &text)
{
    m_proxy->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive));
}

void TreeView::on_treeView_doubleClicked(const QModelIndex &index)
{
    QModelIndex actualIndex = m_proxy->mapToSource(index);
    QObject *p = reinterpret_cast<DataUtils::DataSource*>(actualIndex.internalPointer());
    if (typeid(*p) == typeid(StringTable::Data)) {
        Global::Actions->ConfigureTable->setData(QVariant::fromValue(p));
        Global::Actions->ConfigureTable->trigger();
    } else if (typeid(*p) == typeid(ResourceProject::Data)) {
        Global::Actions->ConfigureProject->setData(QVariant::fromValue(p));
        Global::Actions->ConfigureProject->trigger();
    } else if (typeid(*p) == typeid(StringTable::Entry)) {
        StringTable::Entry *e = static_cast<StringTable::Entry*>(p);
        e->toClipboard();
    }
}
} // namespace ProjectContainer
