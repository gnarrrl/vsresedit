
#include "ProjectContainer.h"
#include "Project.h"
#include "GlobalData.h"
#include "Utils.h"
#include "ChangeNotify.h"

#include <QSettings>
#include <QFile>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <typeinfo>
#include <QApplication>

namespace ProjectContainer
{
ChangeNotifyManager* ChangeNotifier;

///////////////////////////////////////////////////////////////////////////////
// Data

Data::Data(QObject *parent) :
    DataUtils::DataSource(parent),
    Content(this)
{
}

Data::~Data()
{
}

void Data::clear()
{
    emit beginReset(this);
    enableUpdates(false);
    QList<ResourceProject::Data*> tmp = list();
    foreach (ResourceProject::Data *prj, tmp) {
        deleteProject(prj->id());
    }
    enableUpdates(true);
    emit endReset(this);
    Global::UndoStack->clear();
}

void Data::loadFromSettings()
{
    QSettings settings;
    loadConfig(settings.value("projects/configPath").toString());
    connectChangeNotifier();
    Global::ChangeNotifier->clearModified();

    connect(IgnoreList::ChangeNotifier, SIGNAL(documentModified(bool)),
            ChangeNotifier, SLOT(onDataChanged()), Qt::UniqueConnection);
    connect(UserDb::ChangeNotifier, SIGNAL(documentModified(bool)),
            ChangeNotifier, SLOT(onDataChanged()), Qt::UniqueConnection);
    ChangeNotifier->clearModified();
}

void Data::saveToSettings()
{
    QSettings settings;
    settings.setValue("projects/configPath", m_configPath);
    saveConfig();
}

void Data::loadConfig(QString configPath)
{
    m_configPath = configPath;

    // ignore empty path
    if (m_configPath.isEmpty()) return;

    if (!QFile::exists(m_configPath)) {
        // create new config file
        ChangeNotifier->setDirty();
        saveConfig();
    }

    // load configuration file
    QFile f(m_configPath);
    if (!f.open(QFile::ReadOnly)) return;

    clear();

    QXmlStreamReader xs(&f);
    do {
        // root element
        if (!xs.readNextStartElement()) break;
        if (xs.name().toString() != "vsResEditCfg") break;

        // project elements
        while(xs.readNextStartElement()) {
            QString elemName = xs.name().toString();
            QString prjName = xs.attributes().value("name").toString();
            if (elemName == "vsResProject") {
                ResourceProject::Data *prj = createProject();
                prj->fromXml(&xs);
            } else { break; }
        }

        // ignorelist
        if (!xs.atEnd()) {
            if (xs.name().toString() == "ignoreList") {
                Global::IgnoreList->fromXml(&xs);
            }
        }

        // userlist
        if (!xs.atEnd()) {
            if (xs.name().toString() == "userList") {
                Global::UserDb->fromXml(&xs);
            }
        }
    } while(0);
    emit configLoaded();
    Global::ChangeNotifier->clearModified();
}

void Data::saveConfig()
{
    // only write if changed
    if (!ChangeNotifier->isDocumentModified()) {
        return;
    }

    QFile f(m_configPath);
    if (!f.open(QFile::WriteOnly)) return;

    QXmlStreamWriter xs(&f);
    xs.setAutoFormatting(true);
    xs.setAutoFormattingIndent(1);
    xs.writeStartElement("vsResEditCfg");

    foreach (ResourceProject::Data *prj, list()) {
        xs.writeStartElement("vsResProject");
        prj->toXml(&xs);
        xs.writeEndElement();
    }

    xs.writeStartElement("ignoreList");
    Global::IgnoreList->toXml(&xs);
    xs.writeEndElement();

    xs.writeStartElement("userList");
    Global::UserDb->toXml(&xs);
    xs.writeEndElement();

    xs.writeEndElement();
}

void Data::saveProjectsToDisk()
{
    foreach (ResourceProject::Data *prj, list()) {
        prj->saveToDisk();
    }
    Global::ChangeNotifier->clearModified();
    Global::UndoStack->clear();
}

ResourceProject::Data* Data::createProject()
{
    emit beginInsertRows(list().size(), 1, this);
    ResourceProject::Data *p = new ResourceProject::Data(this);
    map().insert(p->id(), p);
    list().push_back(p);
    connectDataSource(p);
    emit endInsertRows();
    return p;
}

void Data::deleteProject(QString guid)
{
    if (ResourceProject::Data *p = projectByGuid(guid)) {
        int idx = list().indexOf(p);
        if (idx == -1) return;
        emit beginRemoveRows(idx, 1, this);
        list().removeAt(idx);
        map().remove(p->id());
        delete p;
        emit endRemoveRows();
    }
    Global::UndoStack->clear();
}

ResourceProject::Data* Data::projectByGuid(QString guid)
{
    ResourceProject::Data *rp = 0;
    QHash<QString, ResourceProject::Data*>::const_iterator i = map().find(guid);
    if (i != map().end()) {
        rp = i.value();
    }
    return rp;
}

void Data::refreshProjects()
{
    QList<ResourceProject::Data*> &d = list();
    foreach (ResourceProject::Data *p, d) {
        p->refreshChangesFromDisk();
    }
}

///////////////////////////////////////////////////////////////////////////////
// TreeModel

ContentModel::ContentModel(Data *data)
    : DataUtils::Model(data)
{
}

int ContentModel::rowInParent(DataUtils::DataSource *child) const
{
    int idx = -1;
    DataUtils::DataSource *parent = parentOfChild(child);
    if (parent == 0) { // child = project, parent = container
        idx = dataSource<Data>()->projectList().indexOf(static_cast<ResourceProject::Data*>(child));
    } else if (ResourceProject::Data *p = dynamic_cast<ResourceProject::Data*>(parent)) {
        idx = p->stringTableList().indexOf(static_cast<StringTable::Data*>(child));
    } else if (StringTable::Data *c = dynamic_cast<StringTable::Data*>(parent)) {
        idx = c->entries().indexOf(static_cast<StringTable::Entry*>(child));
    }
    return idx;
}

DataUtils::DataSource* ContentModel::childInRow(DataUtils::DataSource *parent, int row) const
{
    if (!parent) {
        return dataSource<Data>()->projectList().at(row);
    } else if (ResourceProject::Data *p = dynamic_cast<ResourceProject::Data*>(parent)) {
        return p->stringTableList().at(row);
    } else if (StringTable::Data *p = dynamic_cast<StringTable::Data*>(parent)) {
        return p->entries().at(row);
    }
    return 0;
}

DataUtils::DataSource* ContentModel::parentOfChild(DataUtils::DataSource *child) const
{
    if (typeid(*child) == typeid(ResourceProject::Data)) {
        return 0;
    } else {
        return static_cast<DataUtils::DataSource*>(child->parent());
    }
}

int ContentModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()) return dataSource<Data>()->projectList().size();
    QObject *obj = static_cast<QAbstractItemModel*>(parent.internalPointer());
    if (ResourceProject::Data *d = dynamic_cast<ResourceProject::Data*>(obj)) {
        return d->stringTableList().size();
    } else if (StringTable::Data *d = dynamic_cast<StringTable::Data*>(obj)) {
        return d->entries().size();
    }
    return 0;
}

int ContentModel::columnCount(const QModelIndex &parent) const
{
    (void)parent;
    return 1;
}

QVariant ContentModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();
    QObject *obj = static_cast<QAbstractItemModel*>(index.internalPointer());
    if (ResourceProject::Data *d = dynamic_cast<ResourceProject::Data*>(obj)) {
        switch(role) {
        case Qt::DisplayRole: return QString("%1 (%2)").arg(d->name()).arg(d->stringTableList().size());
        case Qt::EditRole: return d->name();
        case Qt::DecorationRole: return Global::Icons->get(Global::Icons->Package);
        }
    } else if (StringTable::Data *d = dynamic_cast<StringTable::Data*>(obj)) {
        switch(role) {
        case Qt::DisplayRole:
            if (d->entries().size() > 1) return QString("%1 (%2)").arg(d->name()).arg(d->entries().size());
            // else fallthrough (do not display count is only one item)
        case Qt::EditRole: return d->name();
        case Qt::DecorationRole: return Global::Icons->get(Global::Icons->Table);
        }
    } else if (StringTable::Entry *d = dynamic_cast<StringTable::Entry*>(obj)) {
        switch(role) {
        case Qt::DisplayRole: return QString("%1 = \"%2\" (%3)").arg(d->id()).arg(d->text()).arg(d->value());
        case Qt::DecorationRole: return Global::Icons->get(Global::Icons->Pencil);
        }
    }
    return QVariant();
}

bool ContentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) return false;
    if (role != Qt::EditRole) return false;

    QObject *obj = static_cast<QAbstractItemModel*>(index.internalPointer());
    if (ResourceProject::Data *d = dynamic_cast<ResourceProject::Data*>(obj)) {
        d->setName(value.toString());
        ChangeNotifier->setDirty();
    } else if (StringTable::Data *d = dynamic_cast<StringTable::Data*>(obj)) {
        d->setName(value.toString());
    } else {
        return false;
    }
    return true;
}

QVariant ContentModel::headerData(int /*section*/, Qt::Orientation /*orientation*/, int /*role*/) const
{
    return QVariant();
}

Qt::ItemFlags ContentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) return Qt::NoItemFlags;
    QObject *obj = static_cast<QAbstractItemModel*>(index.internalPointer());
    Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    if (!dynamic_cast<StringTable::Entry*>(obj)) {
        flags |= Qt::ItemIsEditable;
    }
    return flags;
}

void ContentModel::onBeginReset(DataUtils::DataSource *source)
{
    if (typeid(*source) == typeid(Data) ||
        typeid(*source) == typeid(ResourceProject::Data))
    {
        beginResetModel();
    }
}

void ContentModel::onEndReset(DataUtils::DataSource *source)
{
    if (typeid(*source) == typeid(Data) ||
        typeid(*source) == typeid(ResourceProject::Data))
    {
        endResetModel();
    }
}
}
