#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QModelIndex>

namespace Utils
{
    QString makeUniqueObjectName(QObject *parent, QString base);
}

#endif // UTILS_H
