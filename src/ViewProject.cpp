#include "ViewProject.h"
#include "ui_ViewProjects.h"
#include "Project.h"
#include "GlobalData.h"

#include <QDataWidgetMapper>
#include <QFileDialog>

namespace ResourceProject
{
View::View(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigureProjectView)
{
    ui->setupUi(this);
    m_mapper = new QDataWidgetMapper(this);
    m_data = 0;
}

View::~View()
{
    delete ui;
}

void View::setData(ResourceProject::Data *prj)
{
    if (m_data) {
        disconnect(m_data, 0, parentWidget(), 0);
    }
    m_data = prj;
    m_mapper->setModel(&prj->Properties);
    m_mapper->addMapping(ui->editResourcePath, ResourceProject::PropertyModel::ResourcePath);
    m_mapper->addMapping(ui->editHeaderPath, ResourceProject::PropertyModel::HeaderPath);
    m_mapper->toFirst();
    connect(prj, SIGNAL(destroyed()), parentWidget(), SLOT(close()));
}

void View::on_btnSelResourcePath_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                tr("Specify path of .rc file"),  QString(),
                tr("Resource Files (*.rc)"));
    if (!fileName.isEmpty()) {
        if (ResourceProject::PropertyModel *model = static_cast<ResourceProject::PropertyModel*>(m_mapper->model())) {
            model->setData(model->index(0, ResourceProject::PropertyModel::ResourcePath), fileName);

            int pos = fileName.lastIndexOf("/");
            if (pos != -1) {
                QString headerPath = QString("%1/resource.h").arg(fileName.left(pos));
                if (QFile::exists(headerPath)) {
                    model->setData(model->index(0, ResourceProject::PropertyModel::HeaderPath), headerPath);
                }
            }
        }
    }
}

void View::on_btnSelHeaderPath_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                tr("Specify path of .h file"),  QString(),
                tr("Resource Headers (*.h)"));
    if (!fileName.isEmpty()) {
        if (ResourceProject::PropertyModel *model = static_cast<ResourceProject::PropertyModel*>(m_mapper->model())) {
            model->setData(model->index(0, ResourceProject::PropertyModel::HeaderPath), fileName);
        }
    }
}

void View::on_btnReload_clicked()
{
    if (ResourceProject::PropertyModel *model = static_cast<ResourceProject::PropertyModel*>(m_mapper->model())) {
        Data *data = model->dataSource<Data>();
        data->setResourcePath(model->data(model->index(0, ResourceProject::PropertyModel::ResourcePath)).toString());
        data->setHeaderPath(model->data(model->index(0, ResourceProject::PropertyModel::HeaderPath)).toString());
        data->loadFromDisk();
    }
}

void View::on_btnReset_clicked()
{
    if (ResourceProject::PropertyModel *model = static_cast<ResourceProject::PropertyModel*>(m_mapper->model())) {
        model->setData(model->index(0, ResourceProject::PropertyModel::ResourcePath),
                       model->dataSource<ResourceProject::Data>()->resourcePath());
        model->setData(model->index(0, ResourceProject::PropertyModel::HeaderPath),
                       model->dataSource<ResourceProject::Data>()->headerPath());
    }
}

} // namespace ResourceProject
