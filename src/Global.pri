HEADERS += \
    Utils.h \
    GlobalData.h \
    GlobalActions.h \
    DataUtils.h \
    IconLibrary.h \
    StyleManager.h \
    ChangeNotify.h

SOURCES += \
    Utils.cpp \
    GlobalData.cpp \
    GlobalActions.cpp \
    IconLibrary.cpp \
    StyleManager.cpp \
    DataUtils.cpp \
    ChangeNotify.cpp
