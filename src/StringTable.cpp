#include "StringTable.h"
#include "Project.h"
#include "GlobalData.h"

#include <QDockWidget>
#include <typeinfo>
#include <QClipboard>
#include <QApplication>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>

namespace StringTable
{
///////////////////////////////////////////////////////////////////////////////
// Data

Data::Data(ResourceProject::Data *parent) :
    DataUtils::DataSource(parent),
    Content(this)
{
    m_view = 0;
}

void Data::clear()
{
    emit beginReset(this);
    qDeleteAll(list());
    list().clear();
    emit endReset(this);
}

bool Data::hasEntryWithIndex() const
{
    bool foundWithIndex = false;
    foreach (Entry *e, list()) {
        QString s(e->id());
        if (s.remove(QRegExp("[0-9]{0,1}$")).length() != e->id().length()) {
            foundWithIndex = true;
            break;
        }
    }
    return foundWithIndex;
}

void Data::setName(QString s)
{
    QString prevName = m_name;
    emit nameAboutToChange();
    emit beginReset(this);

    m_name = s;

    // update ids of entries
    QString replaceRegExp = QString("^%1").arg(prevName);
    foreach (Entry *e, list()) {
        QString s(e->id());
        s.replace(QRegExp(replaceRegExp), m_name);
        e->setId(s);
    }

    emit endReset(this);
    emit nameChanged(m_name);
    emit dataChanged(Scope, this);
    Global::UndoStack->clear();
}

Entry* Data::entryById(QString id) const
{
    QHash<QString, Entry*>::const_iterator i = map().find(id);
    if (i != map().end()) {
        return i.value();
    }
    return 0;
}

Entry* Data::createEntry(QString id)
{
    Entry *e = entryById(id);
    if (!e) {
        emit beginInsertRows(list().size(), 1, this);
        e = new Entry(this);
        if (id.isNull()) {
            id = QString("%1_%2").arg(name()).arg(privateNextEntryIndex(), 3, 10, QChar('0'));
        }
        e->setId(id);

        ResourceProject::Data *project = static_cast<ResourceProject::Data*>(parent());
        e->setValue(project->nextValue());

        list().push_back(e);
        map().insert(id, e);
        m_deletedEntries.remove(id);
        connectDataSource(e);
        emit endInsertRows();
    }
    return e;
}

void Data::deleteEntry(Entry *e)
{
    int idx = list().indexOf(e);
    if (idx == -1) return;
    emit beginRemoveRows(idx, 1, this);
    list().removeAt(idx);
    map().remove(e->id());
    m_deletedEntries.insert(e->id());
    delete e;
    emit endRemoveRows();
}

int Data::privateNextEntryIndex()
{
    int nextIndex = 0;
    QString id, numPart;
    foreach (Entry *e, list()) {
        id = e->id();
        id.remove(QRegExp("[0-9]+$"));
        int numDigits = e->id().length() - id.length();
        nextIndex = qMax(e->id().right(numDigits).toInt()+1, nextIndex);
    }
    return nextIndex;
}

void Data::setView(QDockWidget *view)
{
    if (m_view) return;
    m_view = view;
    if (m_view) {
        connect(m_view, SIGNAL(destroyed()), SLOT(onViewDestroyed()));
        connect(this, SIGNAL(newScopeValue(QString)), SLOT(onUpdateViewTitle()));
        connect(this, SIGNAL(destroyed()), m_view, SLOT(close()));
        connect(this, SIGNAL(nameChanged(QString)), SLOT(onUpdateViewTitle()));
    }
}

QString Data::path() const
{
    ResourceProject::Data *prj = static_cast<ResourceProject::Data*>(parent());
    return QString("%1/%2").arg(prj->name()).arg(name());
}

void Data::toXml(QXmlStreamWriter *xs)
{
    foreach (const QString &s, m_deletedEntries) {
        xs->writeTextElement("delEntry", s);
    }
}

void Data::fromXml(QXmlStreamReader *xs)
{
    while (xs->readNextStartElement()) {
        QString name = xs->name().toString();
        if (name != "delEntry") break;
        m_deletedEntries.insert(xs->readElementText());
    }
}

void Data::onUpdateViewTitle()
{
    if (QDockWidget *p = view()) {
        QString s = path();
        p->setWindowTitle(s);
        p->widget()->setProperty("initViewData", s);
    }
}

void Data::sortedEntries(QList<Entry*> *sortedEntries,
                         Entry::PropertyId pid) const
{
    *sortedEntries = entries();
    switch(pid) {
    case Entry::Id:    qSort(sortedEntries->begin(), sortedEntries->end(), Entry::lessThanId); break;
    case Entry::Text:  qSort(sortedEntries->begin(), sortedEntries->end(), Entry::lessThanText); break;
    case Entry::Value: qSort(sortedEntries->begin(), sortedEntries->end(), Entry::lessThanValue); break;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Entry

void Entry::setId(QString s)
{
    if (m_id == s) return;
    m_id = s;
    emit dataChanged(Id, this);
}

void Entry::setText(QString s)
{
    if (m_text == s) return;
    m_text = s;
    emit dataChanged(Text, this);
}

void Entry::setValue(int n)
{
    if (m_value == n) return;
    m_value = n;
    emit dataChanged(Value, this);
}

void Entry::toClipboard() const
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(id());
}

///////////////////////////////////////////////////////////////////////////////
// ContentModel

ContentModel::ContentModel(Data *data)
    : DataUtils::Model(data)
{
}

DataUtils::DataSource* ContentModel::childInRow(DataUtils::DataSource *parent, int row) const
{
    (void)parent;
    return dataSource<Data>()->entries().at(row);
}

QVariant ContentModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::EditRole || role == Qt::DisplayRole) {
        Entry *e = static_cast<Entry*>(index.internalPointer());
        switch(index.column())
        {
        case Id:     return e->id();
        case Text:   return e->text();
        case Define: return e->value();
        case Format:
        {
            // find format specifiers
            QString txt = e->text();
            QString s;
            QRegExp rx("%(\\.{0,1})([0123456789sudxfg]{1,})");
            rx.setCaseSensitivity(Qt::CaseSensitive);
            int pos = 0;
            while ((pos = rx.indexIn(txt, pos)) != -1) {
                s.append(QString("%1 ").arg(txt.mid(pos, rx.matchedLength())));
                pos += rx.matchedLength();
            }
            return s;
        }
            break;
        }
    } else if (role == Qt::ForegroundRole && index.column() == Format) {
        QColor c(Qt::darkGray);
        return QBrush(c);
    }
    return QVariant();
}

bool ContentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (role == Qt::EditRole) {
        Entry *e = static_cast<Entry*>(index.internalPointer());
        switch(index.column())
        {
        case Text:
            if (e->text() != value.toString()) {
                Global::UndoStack->push(
                            new StringTable::UndoCommands::SetEntryText(
                                static_cast<StringTable::Data*>(e->parent()),
                                e, value.toString()));
            }
            break;
        default: return false;
        }
        return true;
    }
    return false;
}

QVariant ContentModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch(section)
        {
        case Id:     return tr("Define");
        case Text:   return tr("Text");
        case Define: return tr("ID");
        case Format: return tr("Format");
        }
    }
    return QVariant();
}

int ContentModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return dataSource<Data>()->entries().size();
}

Qt::ItemFlags ContentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) return Qt::NoItemFlags;
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.column() == Text) {
        flags |= Qt::ItemIsEditable;
    }
    return flags;
}

void ContentModel::onDataChanged(int propertyId, DataUtils::DataSource *source)
{
    Entry *e = dynamic_cast<Entry*>(source);
    if (!e) return;
    int col;
    switch(propertyId)
    {
    default: Q_ASSERT(0);
    case Entry::Id:    col = Id; break;
    case Entry::Text:  col = Text; break;
    case Entry::Value: col = Define; break;
    }

    privateDataChanged(col, source);
}

int ContentModel::rowInParent(DataUtils::DataSource *child) const
{
    if (Entry *e = dynamic_cast<Entry*>(child)) {
        return dataSource<Data>()->entries().indexOf(e);
    } else {
        return -1;
    }
}

DataUtils::DataSource* ContentModel::parentOfChild(DataUtils::DataSource *child) const
{
    (void)child;
    return 0;
}

namespace UndoCommands
{
///////////////////////////////////////////////////////////////////////////////
// Create Entry

CreateEntry::CreateEntry(StringTable::Data *d,
                         QString id,
                         QUndoCommand *parent) :
    QUndoCommand(parent),
    m_stringTable(d),
    m_id(id),
    m_define(-1)
{
}

void CreateEntry::undo()
{
    m_stringTable->deleteEntry(m_stringTable->entryById(m_id));
}

void CreateEntry::redo()
{
    StringTable::Entry *e = m_stringTable->createEntry(m_id);
    m_id = e->id();
    if (m_define == -1) {
        m_define = e->value();
    } else {
        e->setValue(m_define);
    }
    setText(QString("(add) %1").arg(m_id));
}

///////////////////////////////////////////////////////////////////////////////
// Delete Entry

DeleteEntry::DeleteEntry(StringTable::Data *d,
                         StringTable::Entry *e,
                         QUndoCommand *parent) :
    QUndoCommand(parent),
    m_stringTable(d),
    m_id(e->id()),
    m_define(-1)
{
    m_cmdSetEntryText = new SetEntryText(m_stringTable, e, "", this);
}

void DeleteEntry::undo()
{
    StringTable::Entry *e = m_stringTable->createEntry(m_id);
    e->setValue(m_define);
    m_cmdSetEntryText->undo();
}

void DeleteEntry::redo()
{
    setText(QString("(del) %1").arg(m_id));
    StringTable::Entry *e = m_stringTable->entryById(m_id);
    m_define = e->value();
    m_cmdSetEntryText->redo();
    m_stringTable->deleteEntry(e);
}

///////////////////////////////////////////////////////////////////////////////
// Set Entry Text

SetEntryText::SetEntryText(StringTable::Data *d,
                           StringTable::Entry *e,
                           QString txt,
                           QUndoCommand *parent) :
    QUndoCommand(parent)
{
    m_stringTable = d;
    m_entryId = e->id();
    m_newText = txt;
    m_oldText = e->text();
}

void SetEntryText::undo()
{
    StringTable::Entry *e = m_stringTable->entryById(m_entryId);
    e->setText(m_oldText);
}

void SetEntryText::redo()
{
    StringTable::Entry *e = m_stringTable->entryById(m_entryId);
    setText(QString("(txt) %1 -> %2").arg(e->id()).arg(m_newText));
    e->setText(m_newText);
}
} // namespace UndoCommands
} // namespace StringTable
