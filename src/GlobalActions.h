#ifndef GLOBAL_ACTIONS_H
#define GLOBAL_ACTIONS_H

#include <QObject>

class QAction;

namespace Global
{
class ActionsObject : public QObject
{
    Q_OBJECT
public:
    explicit ActionsObject(QObject *parent = 0);

    void init();

    QAction *ConfigureTable;
    QAction *ConfigureProject;
    QAction *NewTree;
    QAction *NewProject;
    QAction *SaveProjects;
    QAction *ConfigureIgnoreList;
    QAction *SetConfigPath;
    QAction *ConfigureUserList;
    QAction *About;
    QAction *ShowUndoStack;
    QAction *ToggleFileWatch;
    QAction *ManualRefresh;
    QAction *Tutorial;
    QAction *TipsAndTricks;

protected slots:
    void onActionNewProject();
    void onActionAbout();
    void onActionTutorial();
    void onActionTipsAndTricks();
};
}

#endif // GLOBAL_ACTIONS_H
