#ifndef CONTAINER_H
#define CONTAINER_H

#include <QObject>
#include <QList>
#include <QHash>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QRegExp>

namespace Common
{
///////////////////////////////////////////////////////////////////////////////
// Container

/*!\brief Container template for managing objects by a unique identifier.
 * \details
 * \tparam T Type of object stored in the container.
 *  \c T must implement public or via friend class:
 *  - QString objectId() const
 *  - void setObjectId(QString s)
 * \tparam XmlHandlerType Object type implementing static functions for xml processing.
 *  The XmlHandlerType must implement:
 *  -  static void fromXml(Container<XmlHandlerType> *mgr, QXmlStreamReader*);
 *  -  static void toXml(Container<XmlHandlerType> *mgr, QXmlStreamWriter*);
 */
template<class T>
class Container
{
public:
    typedef Container<T> MyType;

    virtual ~Container() {
        clear();
    }

    Container() {}

    Container(const MyType &other) {
        *this = other;
    }

    /*!\brief Replace the contents of this container with a copy of the contents
     *   of another container.
     */
    MyType& operator=(const MyType &other) {
        clear();
        foreach (T *d, other.objectList()) importObject(*d);
        return *this;
    }

    /*!\brief Import an existing object from another container.
     * \returns a pointer to the newly imported object.
     */
    T* importObject(const T &other) {
        T *d = create();
        *d = other;
        return d;
    }

    /*!\brief Delete all managed objects. */
    void clear() {
        qDeleteAll(m_list);
        m_list.clear();
    }

    //! Get the list of all registered objects.
    const QList<T*>& objectList() const {
        return m_list;
    }

    /*!\brief Create a new object.
     * \returns a pointer to the new object.
     */
    T* create(QObject *parent = 0) {
        T* d = new T(parent);
        m_list.push_back(d);
        return d;
    }

    /*!\brief Delete a single object.
     * \param[in] d Object to delete.
     * \returns true if \c d was found within this
     *  container and actually deleted.
     */
    bool remove(T* d) {
        bool res = m_list.removeOne(d);
        if (res) delete d;
        return res;
    }

    /*!\brief Get the object at position \c pos.
     * \param[in] pos Zero based index.
     * \returns a pointer to the object at position \c pos, or
     *  0 if pos is out of range.
     */
    T* at(int pos) const {
        if (pos < 0 || pos >= m_list.size()) return 0;
        else return m_list.at(pos);
    }

    /*!\brief Get the zero based index of an object within the container.
     * \returns -1 of the object is not found in this container.
     */
    int indexOf(const T *d) const {
        return m_list.indexOf(d);
    }

    /*!\brief Get the zero based index of an object within the container.
     * \returns -1 of the object is not found in this container.
     */
    int indexOf(T *d) const {
        return m_list.indexOf(d);
    }

    /*!\brief Swap the positions of two objects in the container.
     * \returns true if both objects are found and could be swapped.
     */
    bool swap(const T *swap,
              const T *with)
    {
        if (swap == with) return true;
        int a = m_list.indexOf(swap);
        if (a == -1) return false;
        int b = m_list.indexOf(with);
        if (b == -1) return false;
        m_list.swap(m_list.indexOf(a),
                    m_list.indexOf(b));
        return true;
    }

protected:
    QList<T*> m_list;
};

///////////////////////////////////////////////////////////////////////////////
// IdContainer

template<class T, class IdType=QString>
class IdContainer : public Container<T>
{
public:
    typedef IdContainer<T, IdType> MyType;

    enum class ReassignResult {
        //! The desired new type id was free and has been assigned.
        Ok,
        //! The desired id is already in use by another type.
        InUse,
        //! Reassign failed because no valid type was registered with the original id.
        OldIdNotFound
    };

    virtual ~IdContainer() {
        clear();
    }

    IdContainer() {}

    IdContainer(const MyType &other) {
        *this = other;
    }

    /*!\brief Replace the contents of this container with a copy of the contents
     *   of another container.
     */
    MyType& operator=(const MyType &other) {
        clear();
        foreach (T *d, other.objectList()) importObject(*d);
        return *this;
    }

    /*!\brief Import an existing object from another container.
     * \returns a pointer to the newly imported object.
     */
    T* importObject(const T &other) {
        T *d = create(other.objectId());
        *d = other;
        return d;
    }

    /*!\brief Rename and copy properties of an existing object.
     * \details This is useful for updating an existing object
     *  with a modified temporary copy from an edit operation.
     *  The object properties will always be merged, but
     *  renaming it might fail if the desired name is already blocked.
     * \returns true if \c mine was found in this manager and
     *  the merge could be completed.
     */
    bool mergeObject(T &mine, const T &other) {
      if (reassign(mine.objectId(), other.objectId()) == ReassignResult::OldIdNotFound)
        return false;

      // must copy everything but the name
      mine = other;
      return true;
    }

    /*!\brief Delete all managed objects. */
    void clear() {
        Container<T>::clear();
        m_hash.clear();
    }

    T* create(IdType id, QObject *parent = 0) {
        T* d = getById(id);
        if (!d) {
            d = new T(parent);
            d->setObjectId(id);
            Container<T>::m_list.push_back(d);
            m_hash.insert(id, d);
        }
        return d;
    }

    T* getById(IdType id) const {
        T* d = 0;
        typename QHash<IdType, T*>::const_iterator i = m_hash.find(id);
        if (i != m_hash.end()) d = i.value();
        return d;
    }

    /*!\brief Assign a different id to a registered object.
     * \param[in] oldId Current id of the object.
     * \param[in] newId New desired id for the object.
     */
    ReassignResult reassign(IdType oldId, IdType newId) {
      T *oldType = getById(oldId);
      if (!oldType) return ReassignResult::OldIdNotFound;
      T *newType = getById(newId);
      if (oldType == newType) return ReassignResult::Ok;
      if (newType) return ReassignResult::InUse;

      m_hash.remove(oldId);
      m_hash.insert(newId, oldType);
      oldType->setObjectId(newId);

      return ReassignResult::Ok;
    }

    /*!\brief Get a unused id based on a baseId which is currently
     *  not registered in this manager and therefore safe to use for
     *  creating new objects.
     */
    QString getUnusedStringId(IdType baseId) const {
      if (m_hash.find(baseId) == m_hash.end()) return baseId;
      int affix = 2;
      QString base = QString("%1").arg(baseId);
      base.remove(QRegExp("_[0-9]{0,}$"));
      QString n(base);
      while(m_hash.find(n) != m_hash.end()) {
          n = QString("%1_%2").arg(base).arg(affix++);
      }
      return n;
    }

protected:
    QHash<IdType, T*> m_hash;
};

} // Common

#endif // CONTAINER_H
