#include "StyleManager.h"

#include <QFile>
#include <QWidget>

StyleManager::StyleManager()
{
    QFile f(":/css/lightblue.css");
    f.open(QFile::ReadOnly);
    m_styleSheet = QString::fromUtf8(f.readAll().data());
}

StyleManager &StyleManager::instance()
{
    static StyleManager singleton;
    return singleton;
}

void StyleManager::applyStyleSheet(QWidget *w)
{
    w->setStyleSheet(m_styleSheet);
}
