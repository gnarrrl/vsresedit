SOURCES += StringTable.cpp \
    Project.cpp \
    ProjectContainer.cpp \
    IgnoreList.cpp \
    UserDb.cpp \
    User.cpp

HEADERS += StringTable.h \
    Project.h \
    ProjectContainer.h \
    IgnoreList.h \
    UserDb.h \
    User.h
