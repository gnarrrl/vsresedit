#ifndef COMMONTYPES_H
#define COMMONTYPES_H

#define CorpSqlDateFormatWrite  QStringLiteral("yyyy-MM-dd hh:mm:ss")
#define CorpNetworkDateFormat   CorpSqlDateFormatWrite
#define CorpSqlDateFormatRead   QStringLiteral("yyyy-MM-ddThh:mm:ss")
#define CorpGuiDateFormat       CorpNetworkDateFormat

#endif // COMMONTYPES_H
