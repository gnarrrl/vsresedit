#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H

#include "Project.h"
#include "ProjectContainer.h"
#include "IconLibrary.h"
#include "GlobalActions.h"
#include "IgnoreList.h"
#include "UserDb.h"
#include "ChangeNotify.h"

#include <QUndoStack>

#define VSRE_VERSION 1
#define VSRE_VERSION_STR "1"

namespace Global
{
extern ProjectContainer::Data*  ResourceProjects;
extern IconLibrary*             Icons;
extern ActionsObject*           Actions;
extern IgnoreList::Data*        IgnoreList;
extern UserDb::Data*            UserDb;
extern QUndoStack*              UndoStack;
extern ChangeNotifyManager*     ChangeNotifier;
}

#endif // GLOBAL_DATA_H
