#ifndef STYLEMANAGER_H
#define STYLEMANAGER_H

#include <QString>
#include <QWidget>

class StyleManager
{
public:
    static StyleManager& instance();

    void applyStyleSheet(QWidget*);

private:
    StyleManager();

    QString m_styleSheet;
};

#endif // STYLEMANAGER_H
