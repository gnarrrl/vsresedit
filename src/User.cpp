#include "User.h"

#include <QXmlStreamWriter>
#include <QXmlStreamReader>

namespace User
{
Data::Data(QObject *parent) :
    DataUtils::DataSource(parent)
{
    m_id = -1;
}

Data::~Data()
{
}

void Data::setName(QString s)
{
    if (m_name == s) return;
    m_name = s;
    emit dataChanged(Name, this);
}

void Data::setId(int n)
{
    if (m_id == n) return;
    m_id = n;
    emit dataChanged(Id, this);
}

void Data::fromXml(QXmlStreamReader *xs)
{
    QXmlStreamAttributes ats = xs->attributes();
    setName(ats.value("name").toString());
    QString s = ats.value("id").toString();
    setId(s.toInt());
}

void Data::toXml(QXmlStreamWriter *xs)
{
    QVariant v(id());
    QString s = v.toString();
    xs->writeAttribute("name", name());
    xs->writeAttribute("id", v.toString());
}
} // namespace USer
