#include "UserDb.h"
#include "GlobalData.h"
#include "ChangeNotify.h"

#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QSettings>
#include <typeinfo>
#include <QMimeData>

namespace UserDb
{
ChangeNotifyManager* ChangeNotifier;

///////////////////////////////////////////////////////////////////////////////
// Data
Data::Data(QObject *parent) :
    DataUtils::DataSource(parent),
    Content(this)
{
    m_activeUser = 0;
}

Data::~Data()
{
    clear();
}

void Data::clear()
{
    m_activeUser = 0;
    emit beginReset(this);
    qDeleteAll(m_userList);
    m_userList.clear();
    emit endReset(this);
}

User::Data* Data::createUser()
{
    emit beginInsertRows(m_userList.size(), 1, this);
    User::Data *user = new User::Data(this);
    user->setName(tr("New User"));
    connectDataSource(user);
    m_userList.push_back(user);
    emit endInsertRows();
    return user;
}

void Data::deleteUser(User::Data *user)
{
    int idx = m_userList.indexOf(user);
    if (idx == -1) return;
    emit beginRemoveRows(idx, 1, this);
    m_userList.removeAt(idx);
    if (user == m_activeUser) {
        m_activeUser = 0;
    }
    delete user;
    emit endRemoveRows();
}

void Data::swap(User::Data *swap,
                User::Data *with)
{
    int first = m_userList.indexOf(swap);
    int second = m_userList.indexOf(with);
    emit beginMoveRow(QPair<int, DataUtils::DataSource*>(first, swap),
                      QPair<int, DataUtils::DataSource*>(second, with));
    m_userList.swap(first, second);
    emit endMoveRow();
}

void Data::setActiveUser(User::Data *user)
{
    m_activeUser = user;
    emit dataChanged(Content.Active, user);
}

void Data::toXml(QXmlStreamWriter *xs)
{
    foreach (User::Data *user, userList()) {
        xs->writeStartElement("user");
        user->toXml(xs);
        xs->writeEndElement();
    }
}

void Data::fromXml(QXmlStreamReader *xs)
{
    clear();
    while(!xs->atEnd()) {
        if (xs->readNextStartElement()) {
            QString name = xs->name().toString();
            if (name != "user") break;
            User::Data *user = createUser();
            user->fromXml(xs);
        }
    }
}

void Data::loadFromSettings()
{
    QSettings settings;
    QString s = settings.value(QString("%1/activeUser").arg(objectName())).toString();
    if (!s.isEmpty()) {
        foreach (User::Data *user, userList()) {
            if (user->name() == s) {
                setActiveUser(user);
                break;
            }
        }
    }
}

void Data::saveSettings()
{
    QSettings settings;
    QString s;
    if (activeUser()) {
        s = activeUser()->name();
    }
    settings.setValue(QString("%1/activeUser").arg(objectName()), s);
}

///////////////////////////////////////////////////////////////////////////////
// ContentModel

ContentModel::ContentModel(Data *data) :
    DataUtils::Model(data)
{
}

int ContentModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return dataSource<Data>()->userList().size();
}

QVariant ContentModel::data(const QModelIndex &index,
                            int role) const
{
    if (!index.isValid())
        return QVariant();

    User::Data *p = static_cast<User::Data*>(index.internalPointer());
    switch(role) {
    case Qt::EditRole:
    case Qt::DisplayRole:
    {
        switch(index.column()) {
        case Name:      return p->name();
        case Id:        return p->id();
        case Active:    return (Global::UserDb->activeUser() == p) ? 1 : 0;
        }
    }
        break;
    case Qt::DecorationRole:
    {
        if (Global::UserDb->activeUser() == p) {
            return Global::Icons->get(Global::Icons->Star);
        }
    }
        break;
    }
    return QVariant();
}

bool ContentModel::setData(const QModelIndex &index,
                           const QVariant &value,
                           int role)
{
    if (!index.isValid())
        return false;

    if (role == Qt::EditRole) {
        User::Data *p = static_cast<User::Data*>(index.internalPointer());
        switch(index.column()) {
        case Name:   p->setName(value.toString()); break;
        case Id:     p->setId(value.toInt()); break;
        case Active:
        {
            if (value.toInt()) {
                Global::UserDb->setActiveUser(p);
                break;
            }
        }
            break;
        default: return false;
        }
        return true;
    }
    return false;
}

QVariant ContentModel::headerData(int section,
                                  Qt::Orientation orientation,
                                  int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch(section) {
        case Name: return tr("Name");
        }
    }
    return QVariant();
}

Qt::ItemFlags ContentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) return Qt::NoItemFlags;
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.column() == Name) {
        flags |= Qt::ItemIsEditable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
    }
    return flags;
}

int ContentModel::rowInParent(DataUtils::DataSource *child) const
{
    if (User::Data *p = qobject_cast<User::Data*>(child)) {
        return dataSource<Data>()->userList().indexOf(p);
    } else {
        return -1;
    }
}

DataUtils::DataSource* ContentModel::childInRow(DataUtils::DataSource *parent,
                                                int row) const
{
    (void)parent;
    Q_ASSERT(row < dataSource<Data>()->userList().size());
    return dataSource<Data>()->userList().at(row);
}

DataUtils::DataSource* ContentModel::parentOfChild(DataUtils::DataSource *child) const
{
    (void)child;
    return 0;
}

Qt::DropActions ContentModel::supportedDropActions() const
{
    return Qt::MoveAction;
}

QStringList ContentModel::mimeTypes() const
{
    QStringList types;
    types << "application/dataSource";
    return types;
}

QMimeData *ContentModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *data = new QMimeData;
    if (indexes.empty()) return data;
    QModelIndex first = indexes.at(0);
    User::Data *user = static_cast<User::Data*>(first.internalPointer());
    size_t ptr = (size_t)user;

    QByteArray d;
    d.append((const char*)&ptr, sizeof(size_t));
    data->setData("application/dataSource", d);
    return data;
}

bool ContentModel::dropMimeData(const QMimeData *data,
                                Qt::DropAction /*action*/,
                                int /*row*/,
                                int /*column*/,
                                const QModelIndex &parent)
{
    QByteArray d = data->data("application/dataSource");
    size_t ptr = *((const size_t*)d.constData());
    User::Data *droppedUser = reinterpret_cast<User::Data*>(ptr);
    User::Data *targetUser = static_cast<User::Data*>(parent.internalPointer());
    dataSource<Data>()->swap(droppedUser, targetUser);
    return true;
}
} // namespace UserDb
