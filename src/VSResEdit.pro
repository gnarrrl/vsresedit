#-------------------------------------------------
#
# Project created by QtCreator 2012-02-02T22:34:15
#
#-------------------------------------------------

QT       += core gui xml network widgets

CONFIG +=

TARGET = VSResEdit
TEMPLATE = app

win32:RC_FILE = res/appicon.rc

RESOURCES += Resources.qrc

include(Logic.pri)
include(Gui.pri)
include(Main.pri)
include(Global.pri)

OTHER_FILES +=

HEADERS += \
    doc/StringEditing.h \
    doc/MainPage.h \
    doc/Finding.h \
    doc/FormatColumn.h \
    doc/Undo.h \
    doc/Clipboard.h \
    doc/LiveDetection.h \
    doc/StringTableManagement.h \
    AbortableObject.h \
    ClosableTabBar.h \
    CommonDefs.h \
    CommonTypes.h \
    ConfigurableObject.h \
    Container.h \
    SharedStrings.h \
    SortProxy.h

