#ifndef VIEW_CONFIG_IGNORELIST_H
#define VIEW_CONFIG_IGNORELIST_H

#include <QWidget>
#include <QVariant>

#include "GlobalData.h"

class QSortFilterProxyModel;

namespace IgnoreList{
class Data;
}

namespace Ui {
class ConfigureIgnoreListView;
}

namespace IgnoreList
{
class View : public QWidget
{
    Q_OBJECT
    
public:
    explicit View(QWidget *parent = 0);
    ~View();
    
    static QString title() { return tr("Configure Ignore List"); }
    static Global::IconLibrary::IconId icon() { return Global::Icons->TableDelete; }

    void setData(IgnoreList::Data*);
    IgnoreList::Data* data() const { return m_data; }

    QString initViewData(QVariant) { return QString(); }

private slots:
    void on_actionDeleteSelected_triggered();
    void on_actionAddEntry_triggered();

    void on_editFilter_textChanged(const QString &arg1);

    void on_btnFromClipboard_clicked();

private:
    IgnoreList::Data *m_data;
    QSortFilterProxyModel *m_proxy;

    Ui::ConfigureIgnoreListView *ui;
};
} // namespace IgnoreList

#endif // VIEW_CONFIG_IGNORELIST_H
