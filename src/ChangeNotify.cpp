
#include "ChangeNotify.h"

///////////////////////////////////////////////////////////////////////////////
// ChangeNotifyManager

ChangeNotifyManager ChangeNotifier;

ChangeNotifyManager::ChangeNotifyManager()
{
    m_documentModified = false;
}

void ChangeNotifyManager::onDataChanged()
{
    m_documentModified = true;
    emit documentModified(true);
}

void ChangeNotifyManager::clearModified()
{
    m_documentModified = false;
    emit documentModified(false);
}

void ChangeNotifyManager::setDirty()
{
    onDataChanged();
}
