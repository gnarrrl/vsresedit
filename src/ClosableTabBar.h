#ifndef CLOSABLETABBAR_H
#define CLOSABLETABBAR_H

#include <QTabWidget>
#include <QTabBar>
#include <QMouseEvent>

namespace Common
{
//! Tab bar that enables tab close on middle mouse button.
class ClosableTabBar : public QTabBar
{
public:
    explicit ClosableTabBar(QWidget* parent=0)
        : QTabBar(parent) {}

    void mouseReleaseEvent (QMouseEvent *event) override {
        if (event->button() == Qt::MidButton)
            emit tabCloseRequested(tabAt(event->pos()));
        else QTabBar::mouseReleaseEvent(event);
    }
};

//! Hack to gain protected access to QTabWidget,
//! which unfortunately is required for setTabBar().
struct TabWidgetAccess : QTabWidget
{
    void setTabBar(QTabBar *bar) {
        QTabWidget::setTabBar(bar);
    }
};
}

#endif // CLOSABLETABBAR_H
