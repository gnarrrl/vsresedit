#include "MainWindow.h"
#include "ui_mainwindow.h"

#include "ViewTree.h"
#include "GlobalData.h"
#include "ViewStringTable.h"
#include "ViewProject.h"
#include "ViewIgnoreList.h"
#include "ViewUserDb.h"

#include "Utils.h"
#include "DataUtils.h"
#include "StyleManager.h"

#include <QTabWidget>
#include <QIcon>
#include <QDockWidget>
#include <QAction>
#include <QTreeView>
#include <QSettings>
#include <typeinfo>
#include <QFileDialog>
#include <QCloseEvent>
#include <QMessageBox>
#include <QTimer>

#include <QUndoView>

class UndoDockWidget : public QUndoView
{
public:
    UndoDockWidget(QWidget *parent) :
        QUndoView(Global::UndoStack, parent)
    {
        QFont font;
        font.setFamily(QString::fromUtf8("Segoe UI"));
        font.setPointSize(9);
        setFont(font);
    }
    static QString title() { return tr("Undo Stack"); }
    static Global::IconLibrary::IconId icon() { return Global::Icons->ArrowUndo; }
    QString initViewData(QVariant) { return QString(); }
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    WindowTitleBase = tr("Visual Studio String Resource Editor");
    StyleManager::instance().applyStyleSheet(this);

    ui->setupUi(this);
    connectActions();
    createToolbar();
    createCentralWidget();

    QSettings settings;
    restoreGeometry(settings.value("mainWnd/geometry").toByteArray());
    createDockWindows();
    restoreState(settings.value("mainWnd/layout").toByteArray());

    if (m_dockWidgets.empty()) {
        createDockWidget<ProjectContainer::TreeView>("dock", QVariant());
    }

    setWindowIcon(Global::Icons->get(Global::Icons->Package));
    setWindowTitle(WindowTitleBase);

    connect(Global::ChangeNotifier, SIGNAL(documentModified(bool)),
            SLOT(onDocumentModified(bool)));
    connect(Global::ResourceProjects, SIGNAL(configLoaded()),
            SLOT(onResourceConfigLoaded()));
    onResourceConfigLoaded();
}

MainWindow::~MainWindow()
{
}

bool MainWindow::maybeSave()
{
    if (Global::ChangeNotifier->isDocumentModified()) {
        QMessageBox msg;
        msg.setWindowTitle(tr("Save"));
        msg.setText(tr("Save changes?"));
        msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Abort);
        msg.setDefaultButton(QMessageBox::Yes);
        msg.setIcon(QMessageBox::Question);
        int res = msg.exec();
        switch(res) {
        case QMessageBox::Yes:
            Global::Actions->SaveProjects->trigger();
        case QMessageBox::No:
            return true;
        default:
        case QMessageBox::Abort:
            return false;
        }
    }
    return true;
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (!maybeSave()) {
        e->ignore();
    } else {
        e->accept();
    }
    QSettings settings;

    settings.setValue("mainWnd/geometry", saveGeometry());
    settings.setValue("mainWnd/layout", saveState());

    settings.beginWriteArray("dockWidgets");
    int idx = 0;
    foreach (QDockWidget *dw, m_dockWidgets) {
        settings.setArrayIndex(idx++);
        settings.setValue("objectName", dw->objectName());
        settings.setValue("typename", typeid(*m_dockedViewMap.find(dw).value()).name());
        settings.setValue("data", dw->widget()->property("initViewData"));
    }
    settings.endArray();

    Global::ResourceProjects->saveToSettings();
    Global::UserDb->saveSettings();
}

void MainWindow::createCentralWidget()
{
    setDockNestingEnabled(true);
    setCentralWidget(0);
}

void MainWindow::createToolbar()
{
    ui->toolBar->addAction(Global::Actions->SaveProjects);
    ui->toolBar->addAction(Global::Actions->NewProject);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(Global::Actions->SetConfigPath);
    ui->toolBar->addAction(Global::Actions->ToggleFileWatch);
    ui->toolBar->addAction(Global::Actions->ManualRefresh);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(Global::Actions->NewTree);
    ui->toolBar->addAction(Global::Actions->ConfigureIgnoreList);
    ui->toolBar->addAction(Global::Actions->ConfigureUserList);
    ui->toolBar->addAction(Global::Actions->ShowUndoStack);
    ui->toolBar->addSeparator();
    ui->toolBar->addAction(Global::Actions->Tutorial);
    ui->toolBar->addAction(Global::Actions->About);
    ui->toolBar->addAction(Global::Actions->TipsAndTricks);
}

void MainWindow::connectActions()
{
    connect(Global::Actions->NewTree, SIGNAL(triggered()), SLOT(onActionNewTree()));
    connect(Global::Actions->ConfigureTable,
            SIGNAL(triggered()), SLOT(onActionConfigureTable()));
    connect(Global::Actions->ConfigureProject,
            SIGNAL(triggered()), SLOT(onActionConfigureProject()));
    connect(Global::Actions->ConfigureIgnoreList,
            SIGNAL(triggered()), SLOT(onActionConfigureIgnoreList()));
    connect(Global::Actions->SetConfigPath,
            SIGNAL(triggered()), SLOT(onActionSelectConfigPath()));
    connect(Global::Actions->ConfigureUserList,
            SIGNAL(triggered()), SLOT(onActionConfigureUserList()));
    connect(Global::Actions->About,
            SIGNAL(triggered()), SLOT(onAbout()));
    connect(Global::Actions->ShowUndoStack,
            SIGNAL(triggered(bool)), SLOT(onActionShowUndoStack()));
}

void MainWindow::createDockWindows()
{
    QSettings settings;
    int size = settings.beginReadArray("dockWidgets");
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        QString type = settings.value("typename").toString();
        QString objName = settings.value("objectName").toString();
        QVariant data = settings.value("data");
        if (type == QString(typeid(ProjectContainer::TreeView).name())) {
           createDockWidget<ProjectContainer::TreeView>(objName, data);
        } else if (type == QString(typeid(StringTable::View).name())) {
           createDockWidget<StringTable::View>(objName, data);
        }
    }
    settings.endArray();
    if (size == 0) {
        Global::Actions->NewTree->trigger();
    }
    Global::Actions->ShowUndoStack->trigger();
}

void MainWindow::onActionNewTree()
{
    createDockWidget<ProjectContainer::TreeView>(Utils::makeUniqueObjectName(this, "dock"), QVariant());
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (QDockWidget *dw = dynamic_cast<QDockWidget*>(obj)) {
        if (event->type() == QEvent::Close) {
            onDockClose(dw);
        }
    }
    return QObject::eventFilter(obj, event);
}

void MainWindow::onDockClose(QDockWidget *dw)
{
    m_dockedViewMap.remove(dw);
    m_dockWidgets.removeOne(dw);
    dw->deleteLater();
}

void MainWindow::onDocumentModified(bool mod)
{
    QString title = windowTitle();
    title.remove("*");
    if (mod) {
        title.insert(0, "*");
    }
    setWindowTitle(title);
}

void MainWindow::onActionConfigureTable()
{
    QAction *action = dynamic_cast<QAction*>(sender());
    if (!action) return;
    if (StringTable::Data *table = dynamic_cast<StringTable::Data*>(action->data().value<QObject*>())) {
        if (QDockWidget *dock = table->view()) {
            dock->raise();
        } else {
            dock = createDockWidget<StringTable::View>(Utils::makeUniqueObjectName(this, "cfgTableView"),
                                                                     table->path());
            dock->setWindowTitle(table->path());
            table->setView(dock);
        }
    }
}

void MainWindow::onActionConfigureProject()
{
    QAction *action = dynamic_cast<QAction*>(sender());
    if (!action) return;
    if (ResourceProject::Data *project = dynamic_cast<ResourceProject::Data*>(action->data().value<QObject*>())) {
        QDockWidget *dock = findChild<QDockWidget*>("cfgProject");
        if (!dock) {
            dock = createDockWidget<ResourceProject::View>("cfgProject", QVariant());
        }
        if (ResourceProject::View *view = dynamic_cast<ResourceProject::View*>(dock->widget())) {
            if (view->data()) {
                dock->disconnect(view->data());
            }
            view->setData(project);
            connect(project, SIGNAL(nameChanged(QString)), dock, SLOT(setWindowTitle(QString)));
        }

        dock->setWindowTitle(project->name());
    }
}

void MainWindow::onActionConfigureIgnoreList()
{
    QDockWidget *dock = findChild<QDockWidget*>("cfgIgnoreList");
    if (!dock) {
        dock = createDockWidget<IgnoreList::View>("cfgIgnoreList", QVariant());
        if (IgnoreList::View *view = dynamic_cast<IgnoreList::View*>(dock->widget())) {
            view->setData(Global::IgnoreList);
        }
    }
}

void MainWindow::onActionSelectConfigPath()
{
    QFileDialog dlg(this,
                    tr("Specify path of config file"),
                    QApplication::applicationDirPath(),
                    tr("Config File (*.xml)"));
    dlg.setDefaultSuffix("xml");
    dlg.selectFile(Global::ResourceProjects->configPath());
    dlg.setFileMode(QFileDialog::AnyFile);
    if (!dlg.exec() == QDialog::Accepted) return;

    QString fileName = dlg.selectedFiles().at(0);
    if (!fileName.isEmpty()) {
        Global::ResourceProjects->loadConfig(fileName);
    }
}

void MainWindow::onActionConfigureUserList()
{
    UserDb::ConfigureDialog dlg;
    dlg.exec();
}

void MainWindow::onActionShowUndoStack()
{
    createDockWidget<UndoDockWidget>("undoView", QVariant());
}

void MainWindow::onResourceConfigLoaded()
{
    // set window caption based on path of loaded configuration
    QString cfgPath(Global::ResourceProjects->configPath());
    if (cfgPath.isEmpty()) {
        cfgPath = tr("<No configuration loaded>");
    }
    QString title = QString("%1 - %2").arg(cfgPath)
            .arg(WindowTitleBase);
    if (Global::ChangeNotifier->isDocumentModified()) {
        title.insert(0, "*");
    }
    setWindowTitle(title);

    // show tutorial automatically if no configuration loaded
    if (Global::ResourceProjects->configPath().isEmpty()) {
        QTimer::singleShot(0, Global::Actions->Tutorial, SLOT(trigger()));
    }
}

void MainWindow::onAbout()
{
}
