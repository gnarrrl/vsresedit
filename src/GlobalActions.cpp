#include "GlobalData.h"
#include "GlobalActions.h"
#include "AboutDialog.h"
#include "TutorialWizard.h"

#include <QAction>
#include <QDesktopServices>
#include <QApplication>
#include <QUrl>

namespace Global
{
ActionsObject::ActionsObject(QObject *parent) :
    QObject(parent)
{
}

void ActionsObject::init()
{
    ConfigureTable = new QAction(Global::Icons->get(Global::Icons->Wrench),
                                 tr("Configure"), this);

    ConfigureProject = new QAction(Global::Icons->get(Global::Icons->Wrench),
                                   tr("Configure"), this);

    NewTree = new QAction(Global::Icons->get(Global::Icons->CogAdd),
                          tr("New Project Tree View"), this);

    NewProject = new QAction(Global::Icons->get(Global::Icons->PackageAdd),
                             tr("Add project"), this);

    SaveProjects = new QAction(Global::Icons->get(Global::Icons->Disk),
                               tr("Save projects\t(Ctrl+S)"), this);
    SaveProjects->setShortcut(QKeySequence(QKeySequence::Save));

    ConfigureIgnoreList = new QAction(Global::Icons->get(Global::Icons->TableDelete),
                               tr("Configure Ignore List"), this);

    SetConfigPath = new QAction(Global::Icons->get(Global::Icons->DatabaseGear),
                             tr("Set configuration path"), this);

    ConfigureUserList = new QAction(Global::Icons->get(Global::Icons->User),
                                    tr("Configure User List"), this);

    About = new QAction(Global::Icons->get(Global::Icons->Information),
                        tr("About Visual Studio Resource Editor"), this);

    ShowUndoStack = new QAction(Global::Icons->get(Global::Icons->ArrowUndo),
                        tr("Show Undo Stack"), this);

    ToggleFileWatch = new QAction(Global::Icons->get(Global::Icons->Magnifier),
                            tr("Enable/Disable live change detection"), this);
    ToggleFileWatch->setCheckable(true);
    ToggleFileWatch->setChecked(true);

    ManualRefresh = new QAction(Global::Icons->get(Global::Icons->Refresh),
                                tr("Manually refresh the projects"), this);

    Tutorial = new QAction(Global::Icons->get(Global::Icons->Help),
                        tr("View the tutorial on how to get started"), this);

    TipsAndTricks = new QAction(Global::Icons->get(Global::Icons->FAQ),
                        tr("Tips and Tricks"), this);

    connect(NewProject, SIGNAL(triggered()), SLOT(onActionNewProject()));
    connect(SaveProjects, SIGNAL(triggered()),
            Global::ResourceProjects, SLOT(saveProjectsToDisk()));
    connect(ManualRefresh, SIGNAL(triggered()),
                Global::ResourceProjects, SLOT(refreshProjects()));
    connect(About, SIGNAL(triggered()), SLOT(onActionAbout()));
    connect(Tutorial, SIGNAL(triggered()), SLOT(onActionTutorial()));
    connect(TipsAndTricks, SIGNAL(triggered()), SLOT(onActionTipsAndTricks()));
}

void ActionsObject::onActionNewProject()
{
    ResourceProject::Data *prj = Global::ResourceProjects->createProject();
    prj->setName(tr("New Project"));
}

void ActionsObject::onActionAbout()
{
    AboutDialog dlg;
    dlg.exec();
}

void ActionsObject::onActionTutorial()
{
    TutorialWizard *w = new TutorialWizard;
    w->setAttribute(Qt::WA_DeleteOnClose);
    w->show();
}

void ActionsObject::onActionTipsAndTricks()
{
    QString s = QString("file:///%1/drocumentation.chm").arg(QApplication::applicationDirPath());
    QDesktopServices::openUrl(QUrl(s));
}
}
