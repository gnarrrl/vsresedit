#ifndef VIEW_TREE_H
#define VIEW_TREE_H

#include <QWidget>
#include <QList>

#include "Project.h"
#include "GlobalData.h"

namespace Ui {
class ProjectTree;
}

class QAction;
class QSortFilterProxyModel;

namespace ProjectContainer
{
class TreeView : public QWidget
{
    Q_OBJECT
    
public:
    explicit TreeView(QWidget *parent = 0);
    ~TreeView();

    static QString title() { return tr("Project Tree"); }
    static Global::IconLibrary::IconId icon() { return Global::Icons->Cog; }

    QString initViewData(QVariant) { return QString(); }

protected:
    virtual void closeEvent(QCloseEvent *e)
    {
        QWidget::closeEvent(e);
    }

public slots:

private slots:
    void onTreeContextMenu(const QPoint&);

    void onActionDeleteProject();
    void onActionCreateTable();
    void onActionDeleteTable();
    void onActionCreateEntry();
    void onActionDeleteEntry();

    void on_lineEdit_textChanged(const QString &arg1);
    void on_treeView_doubleClicked(const QModelIndex &index);

private:
    QSortFilterProxyModel *m_proxy;

    void createActions();

    Ui::ProjectTree *ui;
    QAction *m_actionDeleteProject;
    QAction *m_actionCreateTable;
    QAction *m_actionDeleteTable;
    QAction *m_actionCreateEntry;
    QAction *m_actionDeleteEntry;
};
} // namespace ProjectContainer
#endif // VIEW_TREE_H
