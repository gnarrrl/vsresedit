#ifndef SORTPROXY_H
#define SORTPROXY_H

#include <QSortFilterProxyModel>

namespace Common
{
class RegExpSortProxy : public QSortFilterProxyModel
{
public:
    RegExpSortProxy(QObject *parent)
        : QSortFilterProxyModel(parent)
    {
    }

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
    {
        QModelIndex idx = sourceModel()->index(source_row, 0, source_parent);
        QRegExp rx = filterRegExp();
        rx.setCaseSensitivity(Qt::CaseInsensitive);

        if (int rows = sourceModel()->rowCount(idx)) {
            for (int x=0; x<rows; x++) {
                if (filterAcceptsRow(x, idx)) {
                    return true;
                }
            }
        }

        // check all columns
        bool accept = false;
        for(int x=0; x<sourceModel()->columnCount() && !accept; x++) {
            QModelIndex dataIdx = sourceModel()->index(source_row, x, source_parent);
            QString data = sourceModel()->data(dataIdx).toString();
            accept = data.contains(rx);

        }
        return accept;
    }
};
}

#endif // SORTPROXY_H
