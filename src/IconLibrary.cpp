#include "IconLibrary.h"

namespace Global
{
IconLibrary::IconLibrary(QObject *parent) :
    QObject(parent)
{
}

void IconLibrary::init()
{
    QIcon *icon;

    icon = new QIcon(":/icons/res/package.png");
    m_iconMap.insert(Package, icon);

    icon = new QIcon(":/icons/res/package_add.png");
    m_iconMap.insert(PackageAdd, icon);

    icon = new QIcon(":/icons/res/package_delete.png");
    m_iconMap.insert(PackageDelete, icon);

    icon = new QIcon(":/icons/res/table.png");
    m_iconMap.insert(Table, icon);

    icon = new QIcon(":/icons/res/table_add.png");
    m_iconMap.insert(TableAdd, icon);

    icon = new QIcon(":/icons/res/table_delete.png");
    m_iconMap.insert(TableDelete, icon);

    icon = new QIcon(":/icons/res/pencil.png");
    m_iconMap.insert(Pencil, icon);

    icon = new QIcon(":/icons/res/pencil_add.png");
    m_iconMap.insert(PencilAdd, icon);

    icon = new QIcon(":/icons/res/pencil_delete.png");
    m_iconMap.insert(PencilDelete, icon);

    icon = new QIcon(":/icons/res/add.png");
    m_iconMap.insert(Add, icon);

    icon = new QIcon(":/icons/res/delete.png");
    m_iconMap.insert(Delete, icon);

    icon = new QIcon(":/icons/res/cog.png");
    m_iconMap.insert(Cog, icon);

    icon = new QIcon(":/icons/res/cog_add.png");
    m_iconMap.insert(CogAdd, icon);

    icon = new QIcon(":/icons/res/wrench.png");
    m_iconMap.insert(Wrench, icon);

    icon = new QIcon(":/icons/res/page_attach.png");
    m_iconMap.insert(PageAttach, icon);

    icon = new QIcon(":/icons/res/disk.png");
    m_iconMap.insert(Disk, icon);

    icon = new QIcon(":/icons/res/database_gear.png");
    m_iconMap.insert(DatabaseGear, icon);

    icon = new QIcon(":/icons/res/user.png");
    m_iconMap.insert(User, icon);

    icon = new QIcon(":/icons/res/user_add.png");
    m_iconMap.insert(UserAdd, icon);

    icon = new QIcon(":/icons/res/user_delete.png");
    m_iconMap.insert(UserDelete, icon);

    icon = new QIcon(":/icons/res/star.png");
    m_iconMap.insert(Star, icon);

    icon = new QIcon(":/icons/res/information.png");
    m_iconMap.insert(Information, icon);

    icon = new QIcon(":/icons/res/arrow_undo.png");
    m_iconMap.insert(ArrowUndo, icon);

    icon = new QIcon(":/icons/res/magnifier.png");
    m_iconMap.insert(Magnifier, icon);

    icon = new QIcon(":/icons/res/arrow_refresh.png");
    m_iconMap.insert(Refresh, icon);

    icon = new QIcon(":/icons/res/help.png");
    m_iconMap.insert(Help, icon);

    icon = new QIcon(":/icons/res/HelpFaq64.png");
    m_iconMap.insert(FAQ, icon);
}

const QIcon& IconLibrary::get(IconId id) const
{
    QHash<int, QIcon*>::const_iterator i = m_iconMap.find(id);
    return *(i.value());
}
} // namespace Global
