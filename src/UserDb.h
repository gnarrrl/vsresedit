#ifndef USERDB_H
#define USERDB_H

#include "DataUtils.h"
#include "User.h"
#include "ChangeNotify.h"

#include <QList>

class QXmlStreamReader;
class QXmlStreamWriter;
class QAction;
class QMimeData;

namespace UserDb
{
class Data;

extern ChangeNotifyManager* ChangeNotifier;

///////////////////////////////////////////////////////////////////////////////
// ContentModel
class ContentModel : public DataUtils::Model
{
    Q_OBJECT
public:
    ContentModel(Data*);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const
    { return parent.isValid() ? 0 : 3; }
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    virtual Qt::DropActions supportedDropActions() const;
    virtual QStringList mimeTypes() const;
    virtual QMimeData *mimeData(const QModelIndexList &indexes) const;
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action,
                              int row, int column, const QModelIndex &parent);

    enum Column {
        Name,
        Id,
        Active
    };

protected:
    virtual int rowInParent(DataUtils::DataSource *child) const;
    virtual DataUtils::DataSource* childInRow(DataUtils::DataSource *parent, int row) const;
    virtual DataUtils::DataSource* parentOfChild(DataUtils::DataSource *child) const;
}; // ContentModel

///////////////////////////////////////////////////////////////////////////////
// Data
class Data : public DataUtils::DataSource
{
    Q_OBJECT
    typedef QList<User::Data*> UserList;
    UserList    m_userList;
    User::Data *m_activeUser;
public:
    Data(QObject *parent=0);
    ~Data();

    ContentModel Content;

    void clear();

    User::Data* createUser();
    void deleteUser(User::Data*);
    const UserList& userList() const { return m_userList; }

    void setActiveUser(User::Data *user);
    User::Data* activeUser() const { return m_activeUser; }

    void swap(User::Data *swap, User::Data *with);

    void toXml(QXmlStreamWriter*);
    void fromXml(QXmlStreamReader*);

    void loadFromSettings();
    void saveSettings();
}; // Data

} // namespace UserDb

#endif // USERDB_H
