#ifndef TUTORIALWIZARD_H
#define TUTORIALWIZARD_H

#include <QWizard>

namespace Ui {
class TutorialWizard;
}

class TutorialWizard : public QWizard
{
    Q_OBJECT
    
public:
    explicit TutorialWizard(QWidget *parent = 0);
    ~TutorialWizard();
    
private:
    Ui::TutorialWizard *ui;
};

#endif // TUTORIALWIZARD_H
