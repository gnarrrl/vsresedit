#include "ViewStringTable.h"
#include "ui_ViewStringtable.h"
#include "StringTable.h"
#include "GlobalData.h"

#include <QDataWidgetMapper>
#include <QSortFilterProxyModel>
#include <QSet>
#include <QItemDelegate>
#include <QTextEdit>
#include <QDockWidget>
#include <QSettings>
#include <QKeyEvent>

namespace StringTable
{
///////////////////////////////////////////////////////////////////////////////
// SortProxy

class SortProxy : public QSortFilterProxyModel
{
public:
    SortProxy(QObject *parent)
        : QSortFilterProxyModel(parent)
    {
    }

protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
    {
        QRegExp rx = filterRegExp();
        bool accept = false;
        for(int x=0; x<3 && !accept; x++) {
            QModelIndex idx = sourceModel()->index(source_row, x, source_parent);
            QString data = sourceModel()->data(idx).toString();
            if (data.contains(rx)) {
                accept = true;
            }
        }
        return accept;
    }
};

///////////////////////////////////////////////////////////////////////////////
// MyTextEditor

class MyTextEditor : public QTextEdit
{
    bool m_firstKey;

public:
    MyTextEditor(QWidget *parent = 0)
        : QTextEdit(parent),
          m_firstKey(true)
    {}

protected:
    virtual void keyPressEvent(QKeyEvent *e)
    {
        if (e->key() == Qt::Key_Space && textCursor().atStart()) {
            if (m_firstKey) {
                m_firstKey = false;
                return;
            }
        }
        m_firstKey = false;

        if (e->key() == Qt::Key_Enter ||
            e->key() == Qt::Key_Return)
        {
            Qt::KeyboardModifiers mod = e->modifiers();
            if (mod & Qt::ControlModifier) {
                mod &= ~Qt::ControlModifier;
                e->setModifiers(mod);
            } else {
                parentWidget()->setFocus();
                return;
            }
        }
        QTextEdit::keyPressEvent(e);
    }
};

///////////////////////////////////////////////////////////////////////////////
// MultiLineEditDelegate

class MultiLineEditDelegate : public QItemDelegate
{
public:
    MultiLineEditDelegate(QObject *parent)
        : QItemDelegate(parent)
    {
    }

    QWidget* createEditor(QWidget *parent,
                          const QStyleOptionViewItem &/* option */,
                          const QModelIndex &/* index */) const
    {
        MyTextEditor *editor = new MyTextEditor(parent);
        return editor;
    }

    void setEditorData(QWidget *editor,
                       const QModelIndex &index) const
    {
        QString value = index.model()->data(index, Qt::EditRole).toString();
        value.replace("\\n", "\n");
        QTextEdit *e = static_cast<QTextEdit*>(editor);
        e->setText(value);
    }

    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const
    {
        QTextEdit *e = static_cast<QTextEdit*>(editor);
        QString s = e->toPlainText();
        s.replace("\n", "\\n");
        model->setData(index, s, Qt::EditRole);
    }

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option,
                              const QModelIndex &/* index */) const
    {
        QRect rc(option.rect);
        rc.setHeight(rc.height() * 7);
        editor->setGeometry(rc);
    }
};


///////////////////////////////////////////////////////////////////////////////
// View

View::View(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigureTableView)
{
    m_data = 0;
    m_proxy = 0;

    ui->setupUi(this);

    connect(ui->btnDelEntry, SIGNAL(clicked()), ui->actionDeleteSelected, SLOT(trigger()));
    connect(ui->btnAddEntry, SIGNAL(clicked()), ui->actionAddEntry, SLOT(trigger()));

    ui->btnDelEntry->setEnabled(false);

    const int rowHeight = ui->tableView->fontMetrics().height() + 2;
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    ui->tableView->verticalHeader()->setDefaultSectionSize(rowHeight);
    ui->tableView->verticalHeader()->setStyleSheet(
      "QHeaderView::section {"
         "padding-bottom: 0px;"
         "padding-top: 0px;"
         "padding-left: 0px;"
         "padding-right: 1px;"
         "margin: 0px;"
      "}"
    );

    MultiLineEditDelegate *editor = new MultiLineEditDelegate(ui->tableView);
    ui->tableView->setItemDelegateForColumn(1, editor);
}

View::~View()
{
    if (m_data) {
        QSettings settings;
        settings.setValue(privatSettingsKey(),
                          ui->tableView->horizontalHeader()->saveState());
    }
    delete ui;
}

QString View::privatSettingsKey()
{
    QString dataPath;
    if (m_data) {
        dataPath = m_data->path();
    }
    return QString("%1/%2/state").arg(objectName()).arg(dataPath);
}

void View::setData(StringTable::Data *st)
{
    m_data = st;
    connect(m_data, SIGNAL(destroyed()), SLOT(onDataDeleted()));
    setProperty("initViewData", st->path());
    m_proxy = new SortProxy(this);
    m_proxy->setSourceModel(&m_data->Content);
    m_proxy->setDynamicSortFilter(true);
    ui->tableView->setModel(m_proxy);
    connect(ui->tableView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            SLOT(onTableSelectionChanged(QItemSelection,QItemSelection)),
            Qt::UniqueConnection);
    if (QDockWidget *w = dynamic_cast<QDockWidget*>(parent())) {
        m_data->setView(w);
    }
    QSettings settings;
    ui->tableView->horizontalHeader()->restoreState(
                settings.value(privatSettingsKey()).toByteArray());
}

QString View::initViewData(QVariant data)
{
    QString s = data.toString();
    QStringList sl = s.split("/");
    if (sl.size() != 2) return QString();
    QString prjName = sl[0];
    QString tableName = sl[1];
    QString windowTitle;
    foreach (ResourceProject::Data *prj, Global::ResourceProjects->projectList()) {
        if (prj->name() == prjName) {
            if (StringTable::Data *st = prj->stringTableById(tableName)) {
                setData(st);
                windowTitle = st->path();
                break;
            }
        }
    }
    return windowTitle;
}

void View::on_actionDeleteSelected_triggered()
{
    QModelIndexList sel = ui->tableView->selectionModel()->selectedIndexes();
    if (sel.empty()) return;
    QSet<StringTable::Entry*> delEntries;
    foreach (const QModelIndex &idx, sel) {
        QModelIndex realIdx = m_proxy->mapToSource(idx);
        delEntries.insert(static_cast<StringTable::Entry*>(realIdx.internalPointer()));
    }
    foreach (StringTable::Entry *e, delEntries) {
        Global::UndoStack->push(new StringTable::UndoCommands::DeleteEntry(m_data, e));
    }
}

void View::on_actionAddEntry_triggered()
{
    if (!m_data) return;
    Global::UndoStack->push(new StringTable::UndoCommands::CreateEntry(m_data, QString()));
}

void View::on_editFilter_textChanged(const QString &text)
{
    m_proxy->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive));
}

void View::on_tableView_doubleClicked(const QModelIndex &index)
{
    QModelIndex realIdx = m_proxy->mapToSource(index);
    if (realIdx.column() != 0) return;
    StringTable::Entry *e = reinterpret_cast<StringTable::Entry*>(realIdx.internalPointer());
    e->toClipboard();
}

void View::onTableSelectionChanged(QItemSelection newSel, QItemSelection)
{
    ui->btnDelEntry->setEnabled(!newSel.isEmpty());
}
}

