#ifndef ICONLIBRARY_H
#define ICONLIBRARY_H

#include <QObject>
#include <QHash>
#include <QIcon>

namespace Global
{
class IconLibrary : public QObject
{
    Q_OBJECT
    QHash<int, QIcon*> m_iconMap;
public:
    explicit IconLibrary(QObject *parent = 0);

    void init();

    enum IconId {
        Package,
        Table,
        TableAdd,
        TableDelete,
        Pencil,
        PencilAdd,
        PencilDelete,
        Add,
        PackageAdd,
        PackageDelete,
        Delete,
        Cog,
        CogAdd,
        Wrench,
        PageAttach,
        Disk,
        DatabaseGear,
        User,
        UserAdd,
        UserDelete,
        Star,
        Information,
        ArrowUndo,
        Magnifier,
        Refresh,
        Help,
        FAQ
    };

    const QIcon& get(IconId) const;  
};
}

#endif // ICONLIBRARY_H
