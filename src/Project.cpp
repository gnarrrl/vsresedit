
#include <QUuid>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QRegExp>
#include <QFile>
#include <QTextStream>
#include <QBuffer>
#include <QSet>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFileSystemWatcher>
#include <QDebug>
#include <QTimer>
#include <QApplication>
#include <QDir>
#include <QAction>

#include "Project.h"
#include "GlobalData.h"
#include "ProjectContainer.h"

namespace ResourceProject
{
///////////////////////////////////////////////////////////////////////////////
// Data

Data::Data(QObject *parent)
    : DataUtils::DataSource(parent),
      Properties(this)
{
    m_id = QUuid::createUuid().toString();
    m_watcher = new QFileSystemWatcher(this);
    connect(m_watcher, SIGNAL(fileChanged(QString)),
            SLOT(onFileChanged(QString)));
}

Data::~Data()
{
    clear();
}

void Data::clear()
{
    emit beginReset(this);
    qDeleteAll(list());
    list().clear();
    map().clear();
    emit endReset(this);
}

void Data::loadFromDisk()
{
    emit beginReset(this);
    enableUpdates(false);
    clear();
    privateLoadStrings();
    privateLoadDefines();
    enableUpdates(true);
    emit endReset(this);
}

void Data::saveToDisk()
{
    if (!QFile::exists(m_resourcePath) ||
        !QFile::exists(m_headerPath))
    {
        return;
    }
    privateSaveStrings();
    privateSaveDefines();
}

void Data::privateLoadStrings()
{
    enum ParsingState {
        StateFindStringTable,
        StateFindStringTableBegin,
        StateInsideStringTable
    } state = StateFindStringTable;

    m_tmpResourcePath = m_resourcePath;
    m_tmpHeaderPath = m_headerPath;

    QDir dir(Global::ResourceProjects->configPath());
    QString path = dir.absoluteFilePath(resourcePath());
    QFile file(path);
    if (!file.open(QFile::ReadOnly)) return;
    QTextStream stream(&file);
    stream.setCodec("UTF-8");

    QSet<StringTable::Entry*> seenEntries;
    QSet<StringTable::Data*> seenTables;

    QString line;
    while (!stream.atEnd()) {
        line = stream.readLine().trimmed();
        switch(state)
        {
        case StateFindStringTable:
        {
            // find 'STRINGTABLE'
            if (line.toUpper() == "STRINGTABLE") {
                state = StateFindStringTableBegin;
            }
        }
            break;

        case StateFindStringTableBegin:
        {
            // find 'BEGIN'
            if (line.toUpper() == "BEGIN") {
                state = StateInsideStringTable;
            }
        }
            break;

        case StateInsideStringTable:
        {
            // read next string entry,
            // or leave if a 'END' tag is found
            if (line.toUpper() == "END") {
                state = StateFindStringTable;
            } else {
                int posFirstWs = line.indexOf(QChar(' '));

                QString id;
                if (posFirstWs == -1) {
                    id = line;
                } else {
                    id = line.left(posFirstWs);
                }
                if (id.isEmpty()) break;

                StringTable::Data *table = createStringTable(id);
                seenTables.insert(table);

                int posFirstQuote = line.indexOf(QChar('\"'), posFirstWs);
                int posLastQuote = line.lastIndexOf(QChar('\"'));
                if (posFirstQuote == -1 || posLastQuote == posFirstQuote) {
                    // content must be in the next line
                    line = stream.readLine().trimmed();
                    posFirstQuote = line.indexOf(QChar('\"'));
                    posLastQuote = line.lastIndexOf(QChar('\"'));
                }
                if (posFirstQuote == -1 || posLastQuote == posFirstWs) {
                    break;
                }

                StringTable::Entry *e = table->createEntry(id);
                QString text = line.mid(posFirstQuote+1, posLastQuote-posFirstQuote-1);
                text.replace(QString("\"\""), QChar('\"'));
                e->setText(text);
                seenEntries.insert(e);
            }
        }
            break;
        }
    }

    // delete unseen entries
    QList<StringTable::Data*> delTables;
    QList<StringTable::Entry*> delEntries;
    foreach (StringTable::Data *d, stringTableList()) {
        if (seenTables.find(d) == seenTables.end()) {
            delTables.push_back(d);
        } else {
            foreach (StringTable::Entry *e, d->entries()) {
                if (seenEntries.find(e) == seenEntries.end()) {
                    delEntries.push_back(e);
                }
            }
        }
    }
    foreach (StringTable::Entry *e, delEntries) {
        StringTable::Data *st = static_cast<StringTable::Data*>(e->parent());
        st->deleteEntry(e);
    }
    foreach (StringTable::Data *st, delTables) {
        deleteStringTable(st);
    }

    Global::UndoStack->clear();
}

void Data::privateLoadDefines()
{
    QDir dir(Global::ResourceProjects->configPath());
    QString path = dir.absoluteFilePath(headerPath());
    QFile file(path);
    if (!file.open(QFile::ReadOnly | QFile::Text)) return;
    QTextStream stream(&file);

    QHash<QString, StringTable::Entry*> entryMap;
    foreach (StringTable::Data *st, list()) {
        foreach (StringTable::Entry *e, st->entries()) {
            entryMap.insert(e->id(), e);
        }
    }

    QString line;
    do {
        line = stream.readLine();
        if (!line.isNull()) {
            // strip spaces from left and right
            line = line.trimmed();
            if (line.contains(QRegExp("^#define "))) {
                QStringList l = line.split(QRegExp("\\s+"));
                if (l.size() != 3) continue;

                QString ids = l[1];
                QString def = l[2];

                int value = def.toInt();
                QHash<QString, StringTable::Entry*>::iterator i = entryMap.find(ids);
                if (i != entryMap.end()) {
                    StringTable::Entry *e = i.value();
                    e->setValue(value);
                }
                m_defines.insert(value);
            }
        }
    } while(!line.isNull());
    Global::UndoStack->clear();
}

StringTable::Data* Data::createStringTable(QString scope)
{
    QString noNumbers(scope);
    noNumbers.remove(QRegExp("_[0-9]+$"));
    if (noNumbers.isEmpty()) {
        noNumbers = "IDS_NEW";
    }

    StringTable::Data *st = 0;
    QHash<QString, StringTable::Data*>::Iterator i = map().find(noNumbers);
    if (i == map().end()) {
        emit beginInsertRows(list().size(), 1, this);
        st = new StringTable::Data(this);
        connect(st, SIGNAL(nameAboutToChange()),
                SLOT(onTableNameAboutToChange()));
        connect(st, SIGNAL(nameChanged(QString)),
                SLOT(onTableNameChanged(QString)));
        connect(this, SIGNAL(nameChanged(QString)), st, SLOT(onUpdateViewTitle()));
        st->setName(noNumbers);
        list().push_back(st);
        connectDataSource(st);
        emit endInsertRows();
        emit dataChanged(Name, this);
    } else {
        st = i.value();
    }
    return st;
}

void Data::privateSaveDefines()
{
    enum ParsingState {
        StateBeforeDefines,
        StateInOldDefines,
        StateAppStudioSection
    } state=StateBeforeDefines;

    QFile diskFile(m_headerPath);
    if (!diskFile.open(QFile::ReadOnly)) return;

    QByteArray outData;
    QTextStream streamIn(&diskFile);
    QTextStream streamOut(&outData);

    QString NEWLINE("\n");

    // build set of all entries in all string tables
    QSet<QString> entryIds;

    // build set of recently deleted entries in all string tables,
    // so those are no longer written to the .h file
    // (IDs in the .h that are not found in string tables are
    // streamed out as they are by default)
    QSet<QString> deletedEntries;

    QString line;
    do {
        line = streamIn.readLine();
        if (line.isNull()) break;
        line = line.trimmed();

        switch(state)
        {
        case StateBeforeDefines:
        {
            if (line.contains(QRegExp("^#define "))) {
                // first #define. write all our own defines
                foreach (StringTable::Data *st, list()) {
                    QList<StringTable::Entry*> sortedEntries;
                    st->sortedEntries(&sortedEntries, StringTable::Entry::Value);

                    foreach (const QString &id, st->deletedEntries()) {
                        deletedEntries.insert(id);
                    }
                    st->clearDeletedEntries();

                    foreach (StringTable::Entry *e, sortedEntries) {
                        entryIds.insert(e->id());
                        // ignore defined that are on the ignore list
                        if (Global::IgnoreList->has(e->id())) continue;
                        streamOut << QString("#define %1 %2\n").arg(e->id()).arg(e->value());
                    }
                }

                // only keep if ID is not part of this project,
                // which means it must be a control, menu, etc id
                QString id = line.section(QRegExp("\\s+"), 1, 1);
                if (deletedEntries.find(id) == deletedEntries.end() &&
                    entryIds.find(id) == entryIds.end())
                {
                    streamOut << line << NEWLINE;
                }
                state = StateInOldDefines;
            } else {
                streamOut << line << NEWLINE;
            }
        }
            break;

        case StateInOldDefines:
        {
            if (line.isEmpty()) {
                streamOut << line << NEWLINE;
                state = StateAppStudioSection;
            } else {
                // only keep if ID is not part of this project,
                // which means it must be a control, menu, etc id
                QString id = line.section(QRegExp("\\s+"), 1, 1);
                if (deletedEntries.find(id) == deletedEntries.end() &&
                    entryIds.find(id) == entryIds.end())
                {
                    streamOut << line << NEWLINE;
                }
            }
        }
            break;

        case StateAppStudioSection:
        {
            streamOut << line << NEWLINE;
        }
            break;
        }
    } while(1);

    streamOut.flush();
    diskFile.close();
    if (diskFile.open(QFile::WriteOnly | QFile::Truncate)) {
        diskFile.write(outData);
    }
}

void Data::privateSaveStrings()
{
    enum ParsingState {
        StateBeforeStringTables,
        StateFindStringTable,
        StateInsideStringTable,
        StateAfterStringTables
    } state = StateBeforeStringTables;

    QFile diskFile(m_resourcePath);
    if (!diskFile.open(QFile::ReadOnly)) return;

    QByteArray outData;

    // copy source data until the first STRINGTABLE,
    // then ignore until something other than a stringtable-begin
    // tag is found during StateFindStringTable
    QTextStream streamIn(&diskFile);
    QTextStream streamOut(&outData);

    QString STRINGTABLE("STRINGTABLE\n");
    QString BEGIN("BEGIN\n");
    QString SPACING("    ");
    QString END("END\n");
    QString NEWLINE("\n");

    // use a similar state machine as in privateLoadStrings()
    QString line;
    do {
        line = streamIn.readLine();
        if (line.isNull()) break;
        QString trimmedline = line.trimmed();

        switch(state)
        {
        case StateBeforeStringTables:
        {
            if (trimmedline.toUpper() == "STRINGTABLE") {
                state = StateInsideStringTable;

                // write our own strings, sorted alphabetically
                QList<StringTable::Data*> sortedTables;
                sortedStringTables(&sortedTables);

                QString entryContent;
                foreach (StringTable::Data *st, sortedTables) {
                    if (st->entries().empty()) continue;
                    streamOut << STRINGTABLE;
                    streamOut << BEGIN;
                    QList<StringTable::Entry*> sortedEntries;
                    st->sortedEntries(&sortedEntries, StringTable::Entry::Id);
                    foreach (StringTable::Entry *e, sortedEntries) {
                        // to support the character " in strings, they need to be escaped with two ""
                        entryContent = e->text();
                        entryContent.replace(QChar('\"'), QString("\"\""));
                        streamOut << SPACING << QString("%1 \"%2\"\n").arg(e->id()).arg(entryContent);
                    }
                    streamOut << END;
                    streamOut << NEWLINE;
                }
            } else {
                // still outside string table. copy data.
                streamOut << line << NEWLINE;
            }
        }
            break;

        case StateFindStringTable:
        {
            if (trimmedline.isEmpty()) break;
            if (trimmedline.toUpper() == "STRINGTABLE") {
                state = StateInsideStringTable;
            } else {
                // after last string table
                streamOut << line << NEWLINE;
                state = StateAfterStringTables;
            }
        }
            break;

        case StateInsideStringTable:
        {
            // read next string entry,
            // or leave if a 'END' tag is found
            if (trimmedline.toUpper() == "END") {
                state = StateFindStringTable;
            }
        }
            break;

        case StateAfterStringTables:
        {
            // simply copy any remaining lines
            streamOut << line << NEWLINE;
        }
            break;
        }
    } while(1);

    streamOut.flush();

    // write new contents of .rc file
    diskFile.close();
    if (diskFile.open(QFile::WriteOnly | QFile::Truncate)) {
        diskFile.write(outData);
    }
}

StringTable::Data* Data::stringTableById(QString scope)
{
    StringTable::Data *st = 0;
    QHash<QString, StringTable::Data*>::Iterator i = map().find(scope);
    if (i != map().end()) {
        st = i.value();
    }
    return st;
}

void Data::onTableNameAboutToChange()
{
    StringTable::Data *st = dynamic_cast<StringTable::Data*>(sender());
    if (!st) return;
    map().remove(st->name());
}

void Data::onTableNameChanged(QString)
{
    StringTable::Data *st = dynamic_cast<StringTable::Data*>(sender());
    if (!st) return;
    map().insert(st->name(), st);
}

void Data::deleteStringTable(StringTable::Data *st)
{
    int idx = list().indexOf(st);
    emit beginRemoveRows(idx, 1, this);
    map().remove(st->name());
    list().removeAt(idx);
    delete st;
    emit endRemoveRows();
    emit dataChanged(Name, this);
    Global::UndoStack->clear();
}

void Data::setName(QString s)
{
    m_name = s;
    emit nameChanged(s);
    emit dataChanged(Name, this);
}

void Data::toXml(QXmlStreamWriter *xs)
{
    QString cfgPath = Global::ResourceProjects->configPath();
    QDir dirRc(cfgPath);
    QDir dirH(cfgPath);
    QString relRc = dirRc.relativeFilePath(m_resourcePath);
    QString relH = dirH.relativeFilePath(m_headerPath);

    xs->writeAttribute("name", m_name);
    xs->writeAttribute("resourcePath", relRc);
    xs->writeAttribute("headerPath", relH);

    // store recently deleted but not yet committed entries
    foreach (StringTable::Data *st, list()) {
        if (!st->deletedEntries().empty()) {
            xs->writeStartElement("stringTable");
            xs->writeAttribute("id", st->name());
            st->toXml(xs);
            xs->writeEndElement();
        }
    }
}

void Data::fromXml(QXmlStreamReader *xs)
{
    QXmlStreamAttributes ats = xs->attributes();
    QString rc, h;
    setName(ats.value("name").toString());
    rc = ats.value("resourcePath").toString();
    h = ats.value("headerPath").toString();
    QDir d(Global::ResourceProjects->configPath());
    QString absRc = d.absoluteFilePath(rc);
    QString absH = d.absoluteFilePath(h);
    absRc = QDir::cleanPath(absRc);
    absH  = QDir::cleanPath(absH);
    setResourcePath(absRc);
    setHeaderPath(absH);
    loadFromDisk();

    while (xs->readNextStartElement()) {
        QString s = xs->name().toString();
        if (s != "stringTable") break;
        if (StringTable::Data *st = stringTableById(xs->attributes().value("id").toString())) {
            st->fromXml(xs);
        }
    }
}

void Data::setResourcePath(QString s)
{
    if (m_resourcePath == s) return;
    if (!m_resourcePath.isEmpty()) {
        m_watcher->removePath(m_resourcePath);
    }
    m_resourcePath = s;
    if (!m_resourcePath.isEmpty()) {
        m_watcher->addPath(m_resourcePath);
    }
    ProjectContainer::ChangeNotifier->setDirty();
}

void Data::setHeaderPath(QString s)
{
    if (m_headerPath == s) return;
    if (!m_headerPath.isEmpty()) {
        m_watcher->removePath(m_headerPath);
    }
    m_headerPath = s;
    if (!m_headerPath.isEmpty()) {
        m_watcher->addPath(m_headerPath);
    }
    ProjectContainer::ChangeNotifier->setDirty();
}

void Data::onFileChanged(QString s)
{
    if (!Global::Actions->ToggleFileWatch->isChecked()) return;
    if (s == m_resourcePath) {
        QTimer::singleShot(200, this, SLOT(refreshChangesFromDisk()));
    } else if (s == m_headerPath) {
        QTimer::singleShot(200, this, SLOT(privateLoadDefines()));
    }
}

void Data::refreshChangesFromDisk()
{
    privateLoadStrings();
    privateLoadDefines();
}

int Data::nextValue()
{
    int id = 0;
    if (User::Data *activeUser = Global::UserDb->activeUser()) {
        id = activeUser->id();
    }
    for(;;id++) {
        if (m_defines.find(id) == m_defines.end()) {
            m_defines.insert(id);
            break;
        }
    }
    return id;
}

void Data::sortedStringTables(QList<StringTable::Data*> *l)
{
    *l = list();
    qSort(l->begin(), l->end(), StringTable::Data::lessThanName);
}

void Data::onBeginRemoveRows(int row,
                             int count,
                             DataSource *parent)
{
    if (StringTable::Data *st = qobject_cast<StringTable::Data*>(parent)) {
        if (StringTable::Entry *e = qobject_cast<StringTable::Entry*>(
                    st->Content.childInRow(parent, row)))
        {
            m_defines.remove(e->value());
        }
    }
    DataUtils::DataSource::onBeginRemoveRows(row, count, parent);
}

///////////////////////////////////////////////////////////////////////////////
// PropertyModel

PropertyModel::PropertyModel(Data *data)
    : DataUtils::PropertyModel(data)
{
}

QVariant PropertyModel::data(
        const QModelIndex &index,
        int role) const
{
    if (!index.isValid()) return QVariant();
    if (role == Qt::EditRole || role == Qt::DisplayRole) {
        Data *data = dataSource<Data>();
        switch(index.column())
        {
        case ResourcePath: return data->m_tmpResourcePath;
        case HeaderPath: return data->m_tmpHeaderPath;
        }
    }
    return QVariant();
}

bool PropertyModel::setData(
        const QModelIndex &index,
        const QVariant &value,
        int role)
{
    if (!index.isValid()) return false;
    if (role == Qt::EditRole) {
        Data *data = dataSource<Data>();
        switch(index.column())
        {
        case ResourcePath: data->m_tmpResourcePath = value.toString(); break;
        case HeaderPath:   data->m_tmpHeaderPath = value.toString(); break;
        default: return false;
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

} // namespace ResourceProject
