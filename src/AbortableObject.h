#ifndef ABORTABLEOBJECT_H
#define ABORTABLEOBJECT_H

#include <QMutex>

namespace Common
{
class AbortableObject
{
public:
    bool isAborted()
    {
        m_lock.lock();
        bool abort = m_abort;
        m_lock.unlock();
        return abort;
    }

    void abort()
    {
        m_lock.lock();
        m_abort = true;
        m_lock.unlock();
    }

    void resetAbort()
    {
        m_lock.lock();
        m_abort = false;
        m_lock.unlock();
    }

protected:
    AbortableObject() :
        m_abort(false)
    {}

    volatile bool m_abort;
    QMutex m_lock;
};
} // namespace Common

#endif // ABORTABLEOBJECT_H
