#ifndef SHAREDSTRINGS_H
#define SHAREDSTRINGS_H

#include <QHash>
#include <QString>

namespace Common
{
class SharedStrings
{
public:
    SharedStrings() {}

    inline const QString* sharedString(const QString &s)
    {
        auto i = m_data.find(s);
        if (i == m_data.end()) {
            i = m_data.insert(s, s);
        }
        return &i.value();
    }

    inline const QString* sharedStringLookup(const QString &s) const
    {
        auto i = m_data.find(s);
        return (i == m_data.end()) ? 0 : &i.value();
    }

    inline void clear()
    {
        m_data.clear();
    }

private:
    QHash<QString, QString> m_data;
};
} // namespace Common

#endif // SHAREDSTRINGS_H
