#ifndef STRINGTABLE_H
#define STRINGTABLE_H

#include <QAbstractItemModel>
#include <QList>
#include <QHash>
#include <QVector>
#include <QUndoCommand>
#include <QSet>

#include "DataUtils.h"

class QDockWidget;
class QXmlStreamReader;
class QXmlStreamWriter;

namespace ResourceProject
{
class Data;
}

namespace StringTable
{
class Data;

///////////////////////////////////////////////////////////////////////////////
// ContentModel

class ContentModel : public DataUtils::Model
{
    Q_OBJECT
public:
    ContentModel(Data*);

    enum Columns {
        Id,
        Text,
        Define,
        Format
    };

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const
    {
        return parent.isValid() ? 0 : 4;
    }
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    virtual int rowInParent(DataUtils::DataSource *child) const;
    virtual DataUtils::DataSource* childInRow(DataUtils::DataSource *parent, int row) const;
    virtual DataUtils::DataSource* parentOfChild(DataUtils::DataSource *child) const;

protected slots:
    virtual void onDataChanged(int propertyId, DataUtils::DataSource *source);
}; // ContentModel

///////////////////////////////////////////////////////////////////////////////
// Entry

/*!\brief
 */
class Entry : public DataUtils::DataSource
{
    Q_OBJECT
public:
    explicit Entry(QObject *parent = 0)
        : DataUtils::DataSource(parent)
    {
        m_value = 0;
    }

    const QString& id() const { return m_id; }
    void setId(QString s);

    const QString& text() const { return m_text; }
    void setText(QString s);

    int value() const { return m_value; }
    void setValue(int n);

    void toClipboard() const;

    enum PropertyId {
        Id,     //!< Define, e.g. IDS_TXT_001, stored in the .rc file
        Text,   //!< Text content
        Value   //!< Numeric value of the define
    };

    /*!\brief Compare function for qSort. */
    static bool lessThanId(const Entry *a, const Entry *b) {
        return a->id() < b->id();
    }

    /*!\brief Compare function for qSort. */
    static bool lessThanText(const Entry *a, const Entry *b) {
        return a->text() < b->text();
    }

    /*!\brief Compare function for qSort. */
    static bool lessThanValue(const Entry *a, const Entry *b) {
        return a->value() < b->value();
    }

private:
    QString m_id;
    QString m_text;
    int     m_value;
};

///////////////////////////////////////////////////////////////////////////////
// Data

class Data : public DataUtils::DataSource
{
    Q_OBJECT
    friend class ContentModel;

    enum { last = -1 };

    QString m_name;
    QDockWidget   *m_view;
    QSet<QString>  m_deletedEntries;

    QPair<QHash<QString, Entry*>,
          QList<Entry*> > m_entries;

    QHash<QString, Entry*>& map() { return m_entries.first; }
    const QHash<QString, Entry*>& map() const { return m_entries.first; }

    QList<Entry*>& list() { return m_entries.second; }
    const QList<Entry*>& list() const { return m_entries.second; }

public:
    explicit Data(ResourceProject::Data *parent = 0);
    virtual ~Data() { clear(); }

    ContentModel Content;

    enum Properties {
        Scope
    };

    void clear();
    bool hasEntryWithIndex() const;

    void setName(QString s);
    const QString& name() const { return m_name; }

    const QList<Entry*>& entries() const { return m_entries.second; }
    void sortedEntries(QList<Entry*> *sortedEntries, Entry::PropertyId) const;

    const QSet<QString>& deletedEntries() const { return m_deletedEntries; }
    void clearDeletedEntries() { m_deletedEntries.clear(); }

    Entry* createEntry(QString id);
    void deleteEntry(Entry*);

    Entry* entryById(QString id) const;

    QDockWidget* view() const { return m_view; }
    void setView(QDockWidget *view);

    QString path() const;

    void toXml(QXmlStreamWriter*);
    void fromXml(QXmlStreamReader*);

    static bool lessThanName(const Data *a, const Data *b) {
        return a->name() < b->name();
    }

public slots:
    void onUpdateViewTitle();

signals:
    void newScopeValue(QString);

    void nameAboutToChange();
    void nameChanged(QString);

protected slots:
    void onViewDestroyed() { m_view = 0; }

private:
    int privateNextEntryIndex();
}; // Data

namespace UndoCommands
{
///////////////////////////////////////////////////////////////////////////////
// Create Entry

class CreateEntry : public QUndoCommand
 {
    StringTable::Data  *m_stringTable;
    QString m_id;
    int m_define;
public:
    CreateEntry(StringTable::Data *d,
                QString id,
                QUndoCommand *parent=0);
    virtual void undo();
    virtual void redo();
};

///////////////////////////////////////////////////////////////////////////////
// Set Entry Text

class SetEntryText : public QUndoCommand
 {
    StringTable::Data *m_stringTable;
    QString m_entryId;
    QString m_newText;
    QString m_oldText;
public:
    SetEntryText(StringTable::Data *d,
                 StringTable::Entry *e,
                 QString txt,
                 QUndoCommand *parent=0);
    virtual void undo();
    virtual void redo();
};

///////////////////////////////////////////////////////////////////////////////
// Delete Entry

class DeleteEntry : public QUndoCommand
 {
    StringTable::Data  *m_stringTable;
    QString m_id;
    int m_define;
    SetEntryText *m_cmdSetEntryText;
public:
    DeleteEntry(StringTable::Data *d,
                StringTable::Entry *e,
                QUndoCommand *parent=0);
    virtual void undo();
    virtual void redo();
};
} // namespace UndoCommands
} // namespace StringTable

#endif // STRINGTABLE_H
