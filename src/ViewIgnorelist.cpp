#include "ViewIgnoreList.h"
#include "ui_ViewIgnorelist.h"
#include "IgnoreList.h"

#include <QSortFilterProxyModel>

namespace IgnoreList
{
View::View(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigureIgnoreListView)
{
    ui->setupUi(this);

    connect(ui->btnDelEntry, SIGNAL(clicked()), ui->actionDeleteSelected, SLOT(trigger()));
    connect(ui->btnAddEntry, SIGNAL(clicked()), ui->actionAddEntry, SLOT(trigger()));

    m_data = 0;
    m_proxy = 0;

    const int rowHeight = ui->tableView->fontMetrics().height() + 2;
    ui->tableView->verticalHeader()->setDefaultSectionSize(rowHeight);
    ui->tableView->verticalHeader()->setStyleSheet(
      "QHeaderView::section {"
         "padding-bottom: 0px;"
         "padding-top: 0px;"
         "padding-left: 0px;"
         "padding-right: 1px;"
         "margin: 0px;"
      "}"
    );
}

View::~View()
{
    delete ui;
}

void View::setData(IgnoreList::Data *st)
{
    m_data = st;
    m_proxy = new QSortFilterProxyModel(this);
    m_proxy->setSourceModel(&m_data->Content);
    m_proxy->setDynamicSortFilter(true);
    m_proxy->setFilterKeyColumn(1);
    ui->tableView->setModel(m_proxy);
}

void View::on_actionDeleteSelected_triggered()
{
    QModelIndexList sel = ui->tableView->selectionModel()->selectedIndexes();
    if (sel.empty()) return;
    QSet<QString> delEntries;
    foreach (const QModelIndex &idx, sel) {
        QModelIndex realIdx = m_proxy->mapToSource(idx);
        delEntries.insert(*reinterpret_cast<QString*>(realIdx.internalPointer()));
    }
    foreach (const QString e, delEntries) {
        m_data->remove(e);
    }
}

void View::on_actionAddEntry_triggered()
{
    QString base("AFX_IGNOREME");
    QString s(base);
    int idx = 2;
    while (!m_data->add(s)) {
        s = QString("%1_%2").arg(base).arg(idx++);
    }
}

void View::on_editFilter_textChanged(const QString &text)
{
    m_proxy->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive));
}

void View::on_btnFromClipboard_clicked()
{
    m_data->fromClipboard();
}
} // namespace IgnoreList
