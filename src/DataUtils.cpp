
#include "DataUtils.h"

#include <typeinfo>
#include <QIODevice>

namespace DataUtils
{
///////////////////////////////////////////////////////////////////////////////
// Model

Model::Model(DataSource *data, QObject *parent) :
    QAbstractItemModel(parent)
{
    m_data = data;

    if (data) {
        connect(data, SIGNAL(beginInsertRows(int,int,DataSource*)),
                SLOT(onBeginInsertRows(int,int,DataSource*)));
        connect(data, SIGNAL(beginRemoveRows(int,int,DataSource*)),
                SLOT(onBeginRemoveRows(int,int,DataSource*)));
        connect(data, SIGNAL(endInsertRows()), SLOT(onEndInsertRows()));
        connect(data, SIGNAL(endRemoveRows()), SLOT(onEndRemoveRows()));
        connect(data, SIGNAL(beginReset(DataSource*)), SLOT(onBeginReset(DataSource*)));
        connect(data, SIGNAL(endReset(DataSource*)), SLOT(onEndReset(DataSource*)));
        connect(data, SIGNAL(dataChanged(int,DataSource*)), SLOT(onDataChanged(int,DataSource*)));
        //connect(this, SIGNAL(modified()), data, SLOT(onModified()));
        connect(data, SIGNAL(beginMoveRow(QPair<int,DataSource*>,QPair<int,DataSource*>)),
                SLOT(onBeginMoveRow(QPair<int,DataSource*>,QPair<int,DataSource*>)));
        connect(data, SIGNAL(endMoveRow()), SLOT(onEndMoveRow()));
    }
}

void Model::onBeginInsertRows(int row, int count, DataSource *parent)
{
    beginInsertRows(indexForObject(parent, 0), row, row+count-1);
}

void Model::onEndInsertRows()
{
    endInsertRows();
    emit modified();
}

void Model::onBeginRemoveRows(int row, int count, DataSource *parent)
{
    beginRemoveRows(indexForObject(parent, 0), row, row+count-1);
}

void Model::onEndRemoveRows()
{
    endRemoveRows();
    emit modified();
}

void Model::onBeginMoveRow(QPair<int, DataUtils::DataSource *> source,
                           QPair<int, DataUtils::DataSource *> destination)
{
    QModelIndex srcParent = indexForObject(source.second, 0);
    QModelIndex destParent = indexForObject(destination.second, 0);

    beginMoveRows(srcParent, source.first, source.first,
                  destParent, destination.first);
}

void Model::onEndMoveRow()
{
    endMoveRows();
    emit modified();

}

void Model::onBeginReset(DataSource *source)
{
    if (source == m_data) {
        beginResetModel();
    }
}

void Model::onEndReset(DataSource *source)
{
    if (source == m_data) {
        endResetModel();
        emit modified();
    }
}

void Model::onDataChanged(int propertyId, DataSource *source)
{
    privateDataChanged(propertyId, source);
    emit modified();
}

QModelIndex Model::indexForObject(DataSource *source, int column)
{
    if (!source) return QModelIndex();
    Q_ASSERT(column >= 0 && column < columnCount());

    QList<DataSource*> parentObjects;
    DataSource *p = source;
    while ((p = parentOfChild(p)) != 0) {
        parentObjects.push_back(p);
    }

    QModelIndex idx = QModelIndex();
    for (int x=parentObjects.size(); x>0; x--) {
        p = parentObjects.at(x-1);
        idx = index(rowInParent(p), column, idx);
    }
    int rip = rowInParent(source);
    if (rip != -1) {
        idx = index(rip, column, idx);
    }
    return idx;
}

void Model::privateDataChanged(int column, DataSource *source)
{
    QModelIndex idx = indexForObject(source, column);
    if (idx.isValid()) {
        emit dataChanged(idx, idx);
    }
}

QModelIndex Model::index(
        int row,
        int column,
        const QModelIndex &parent) const
{
    if (column < 0 || column >= columnCount(parent) ||
        row < 0 || row >= rowCount(parent))
    {
        return QModelIndex();
    }
    DataSource *p = 0;
    if (parent.isValid()) {
        p = static_cast<DataSource*>(parent.internalPointer());
    }
    return createIndex(row, column, (void*)childInRow(p, row));
}

QModelIndex Model::parent(const QModelIndex &child) const
{
    if (!child.isValid()) return QModelIndex();
    DataSource *c = static_cast<DataSource*>(child.internalPointer());
    DataSource *p = parentOfChild(c);
    if (!p) return QModelIndex();
    else return createIndex(rowInParent(p), 0, p);
}

void Model::store(StoreFormat fmt, QIODevice *io) const
{
    switch(fmt) {
    case StoreFormat::CSV:
    {
        // csv header
        int cols = columnCount();
        QString line("#");
        for (int col = 0; col < cols; col++) {
            line.append(headerData(col, Qt::Horizontal, Qt::DisplayRole).toString());
            if (col+1 != cols) line.append(',');
        }
        line.append('\n');
        io->write(line.toUtf8());

        // csv data
        int rows = rowCount();
        for (int row = 0; row < rows; row++) {
            line.clear();
            for (int col = 0; col < cols; col++) {
                QString cc(data(index(row, col)).toString());
                if (cc.contains(',')) {
                    cc.insert(0, '\"');
                    cc.append('\"');
                }
                line.append(cc);
                if (col+1 != cols) line.append(',');
            }
            line.append('\n');
            io->write(line.toUtf8());
        }
    }
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
// DataSource

ChangeNotifyManager DataSource::StaticDataSourceChange;

DataSource::DataSource(QObject *parent, ChangeNotifyManager *customManager)
    : QObject(parent)
{
    m_updatesEnabled = true;
    if (parent || customManager) {
        connectChangeNotifier(customManager);
    }

    // connect signals that modify is to the onModified slot
    // to ensure we emit modified() when our data changes.
    // make the connetion queued (async) to have all
    // model related updates be executed first
    // (e.g. objects in tree got deleted), and then
    // the update triggered by the modified signal can
    // do its work on the updated model data
    connect(this, SIGNAL(dataChanged(int,DataSource*)),
            SLOT(onModified()), Qt::QueuedConnection);
    connect(this, SIGNAL(endInsertRows()),
            SLOT(onModified()), Qt::QueuedConnection);
    connect(this, SIGNAL(endRemoveRows()),
            SLOT(onModified()), Qt::QueuedConnection);
    connect(this, SIGNAL(endReset(DataSource*)),
            SLOT(onModified()), Qt::QueuedConnection);
    connect(this, SIGNAL(endMoveRow()),
            SLOT(onModified()), Qt::QueuedConnection);
}

void DataSource::connectDataSource(DataSource *src)
{
    connect(src, SIGNAL(beginInsertRows(int,int,DataSource*)),SLOT(onBeginInsertRows(int,int,DataSource*)));
    connect(src, SIGNAL(beginRemoveRows(int,int,DataSource*)), SLOT(onBeginRemoveRows(int,int,DataSource*)));
    connect(src, SIGNAL(endInsertRows()), SLOT(onEndInsertRows()));
    connect(src, SIGNAL(endRemoveRows()), SLOT(onEndRemoveRows()));
    connect(src, SIGNAL(dataChanged(int,DataSource*)), SLOT(onDataChanged(int,DataSource*)));
    connect(src, SIGNAL(beginReset(DataSource*)), SLOT(onBeginReset(DataSource*)));
    connect(src, SIGNAL(endReset(DataSource*)), SLOT(onEndReset(DataSource*)));
    connect(src, SIGNAL(beginMoveRow(QPair<int,DataSource*>,QPair<int,DataSource*>)),
            SLOT(onBeginMoveRow(QPair<int,DataSource*>,QPair<int,DataSource*>)));
    connect(src, SIGNAL(endMoveRow()), SLOT(onEndMoveRow()));
}

void DataSource::connectChangeNotifier(ChangeNotifyManager *mgr)
{
    if (!mgr) {
        mgr = &StaticDataSourceChange;
    }
    connect(this, SIGNAL(modified()), mgr, SLOT(onDataChanged()),
            Qt::UniqueConnection);
}

void DataSource::onBeginReset(DataSource *source)
{
    if (!updatesEnabled()) return;
    emit beginReset(source);
}

void DataSource::onEndReset(DataSource *source)
{
    if (!updatesEnabled()) return;
    emit endReset(source);
}

void DataSource::onBeginInsertRows(int row, int count, DataSource *parent)
{
    if (!updatesEnabled()) return;
    emit beginInsertRows(row, count, parent);
}

void DataSource::onEndInsertRows()
{
    if (!updatesEnabled()) return;
    emit endInsertRows();
}

void DataSource::onBeginRemoveRows(int row, int count, DataSource *parent)
{
    if (!updatesEnabled()) return;
    emit beginRemoveRows(row, count, parent);
}

void DataSource::onEndRemoveRows()
{
    if (!updatesEnabled()) return;
    emit endRemoveRows();
}

void DataSource::onDataChanged(int propertyId, DataSource *source)
{
    if (!updatesEnabled()) return;
    emit dataChanged(propertyId, source);
}

void DataSource::onModified()
{
    emit modified();
}

void DataSource::onBeginMoveRow(QPair<int, DataSource *> source,
                                QPair<int, DataSource *> destination)
{
    if (!updatesEnabled()) return;
    emit beginMoveRow(source, destination);
}

void DataSource::onEndMoveRow()
{
    if (!updatesEnabled()) return;
    emit endMoveRow();
}

///////////////////////////////////////////////////////////////////////////////
// PropertyModel

int PropertyModel::rowInParent(DataSource *child) const
{
    return (child == dataSource<DataSource>()) ? 0 : -1;
}

Qt::ItemFlags PropertyModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) return Qt::NoItemFlags;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

} // namespace DataUtils
