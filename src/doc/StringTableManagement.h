/*!\page PageStringTableMgm String Table management

\tableofcontents
- \ref StringTableCreate
- \ref StringTableManage

\section StringTableCreate Create string tables
String tables are created via the context menu on projects in the
Project Tree. When adding a new table, it's name will be IDS_NEW
by default. If there is already a table existing with that name,
no new table will be created.


\section StringTableManage Table and string entry naming
- String table names are changed by the common tree entry edit key F2.

- String entry names are designed to be named in the format
<custom name>_<%03d running index> to identify certain sections
strings belong to, like a certain GUI for
instance. Defines without running indices are supported,
but only as compatibility with existing resource files.
It is not possible to create MY_RANDOM_STRING within the editor.
Instead, you could create a string table names MY_RANDOM_STRING and
create an entry within it that is MY_RANDOM_STRING_000.

- Renaming a string table will automatically replace the portion of the
original string table name with the new table name.

*/
