/*!\page PageClipboard Using the clipboard
When double-clicking an entry in the Project Tree or the contents of the
'Define' column in a String Table View, the define name (e.g. IDS_MYSTRING_001)
is copied to the clipboard.

This makes copy/pasting defines from the resource editor to the code
quick and efficient.
*/
