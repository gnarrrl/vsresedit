/*!\mainpage Tips & Tricks for the Droconut Visual Studio String Resource Editor
\section SecIntro Introduction
This guide contains a collection of useful tips on how to effectively use this software.

\section MainStringTableMgm String Table management
- \ref StringTableCreate
- \ref StringTableManage

\section MainStringEdit String editing
- \ref StringEditSpace
- \ref StrgEditEnter

\section MainFinding Finding stuff
- \ref FindingRegExp
- \ref FindingByResourceId
- \ref FindingAffinity

\section MainFormatColumn Format string column
- \ref PageFormatColumn

\section MainClipboard Using the clipboard
- \ref PageClipboard

\section MainUndo Undo/Redo
- \ref PageUndo

\section MainLiveDetection Tricks with live detection
- \ref PageLiveDetection

*/

