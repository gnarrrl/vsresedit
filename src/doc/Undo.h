/*!\page PageUndo Undo/Redo
All changes made to contents of String Tables are tracked and
displayed in the Undo Stack. Navigating this list will undo/redo
the steps you have taken. Saving the project will clear the Undo Stack.
*/
