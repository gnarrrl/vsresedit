/*!\page PageLiveDetection Tricks with live detection
By default the live file change detection is active and any changes
made to managed .rc and .h files will instantly be merged into the
current data set. So you can keep the editor open while accepting
the latest changes from your source code repository. They will
seamlessly be integrated.

This can be disabled/enabled by toggling the magnifying glass
tool button.

When will disabling this feature ever be useful?
Well, if you have local changes in your resource editor (i.e. new strings)
while at the same time having incoming changes from the repository
that would heavily conflict if you were to save your changes,
and you only need a specific part of the repository changes that are
for example modified dialog resources (the MS resource editor will
re-arrange the contents of the .rc file every time you save it in
Visual Studio), you can disable the live detection, accept the changes
from the repository, copy & paste the parts you want to keep to
a text editor, 'Save' in the Droconut Resource Editor, re-enable
live detection and manually paste the changed dialog resources
via text editor back to the .rc file.
This technique will save you lots of pain merging conflicts, if
you ever run into that kind of scenario.

*/
