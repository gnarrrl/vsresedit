/*!\page PageFinding Finding stuff

\tableofcontents
- \ref FindingRegExp
- \ref FindingByResourceId
- \ref FindingAffinity

\section FindingRegExp Regular expressions
All "Finder" fields take non case-sensitive regular expressions to find
what you're looking for.
For detailed information about the capabilities of Qt's RegExp, see
http://doc.qt.nokia.com/4.7-snapshot/qregexp.html#details


\section FindingByResourceId Find by resource ID
- The easy solution: Simply enter the resource ID you are looking for in the Finder
above the Project Tree, eg 35000.

- The refined solution: To exclude all instances where the number value
of the resource you are looking for is occurring also in the string content,
use this RegExp (again for the example ID of 35000): \(35000\)$


\section FindingAffinity Find strings over multiple projects
When looking for a particular piece of text, e.g. if you don't know where
that piece of source code is that produces this nasty message box someone
is asking about, the fastest way to find the code is more often than not
via the text contents of that said message box. To instantly locate it
anywhere in all your managed projects, simply put in the piece of text
into the Finder above the Project tree.
What will remain are all strings in all your projects where that
piece of text is found.
If you then have located the exact string table that contains your
target string it is advisable to either copy & paste the search text from the
Project Tree Finder to the Finder above the String Table or double-click
the entry in the Project Tree and use Paste on the String Table Finder.
The first will put in the search text, the other will directly match
the exact string ID.


*/
