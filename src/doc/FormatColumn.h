
/*!\page PageFormatColumn Format string column
The "Format" column in the String Table Views shows a summary of all
format strings (%s, %d, %.02f, ...) in that particular entry.
This is especially useful when having multiple formats in a large
multi-line string. When coding the contents of such a string, you then
only need to look at the format column to get a quick and easy overview.

Having the format column also makes it easier to find and sort based on
format specifiers.
*/
