/*!\page PageStringEditing String editing

\tableofcontents
- \ref StringEditSpace
- \ref StrgEditEnter

\section StringEditSpace Use 'Space' to begin editing
The SPACE key starts an edit operation without changing the contents of
the cell. In combination with the Arrow-Keys for up and down cell
navigation this is a very effective method of editing multiple strings
without the need to use the mouse.

\section StrgEditEnter Strg+Enter for multi-line
Add multiple lines to your strings by using Strg+Enter.
If you edit multiple lines, newline characters ('\\n') are automatically
converted added to the string as necessary, and are omitted during editing.

*/
