#ifndef CONFIGURABLEOBJECT_H
#define CONFIGURABLEOBJECT_H

namespace Common
{
template<class ConfigType>
class ConfigurableObject
{
public:
    const ConfigType& configuration() const { return m_cfg; }
    void configure(const ConfigType &cfg) {
        m_cfg = cfg;
        onConfigChanged();
    }

protected:
    ConfigurableObject(){}
    virtual void onConfigChanged() {}

protected:
    ConfigType m_cfg;
};
}

#endif // CONFIGURABLEOBJECT_H
