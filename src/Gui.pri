SOURCES += ViewProject.cpp \
           ViewStringtable.cpp \
           ViewTree.cpp \
           ViewIgnorelist.cpp \
           ViewUserdb.cpp \
           AboutDialog.cpp \
           TutorialWizard.cpp

HEADERS += ViewProject.h \
           ViewStringtable.h \
           ViewTree.h \
           ViewIgnorelist.h \
           ViewUserdb.h \
           AboutDialog.h \
           TutorialWizard.h

FORMS += \
    ViewTree.ui \
    ViewProjects.ui \
    ViewStringtable.ui \
    ViewIgnorelist.ui \
    ViewUserdb.ui \
    AboutDialog.ui \
    TutorialWizard.ui
