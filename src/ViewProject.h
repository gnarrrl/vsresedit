#ifndef VIEW_CONFIG_PROJECT_H
#define VIEW_CONFIG_PROJECT_H

#include <QWidget>
#include <QVariant>

#include "GlobalData.h"

class QDataWidgetMapper;

namespace ResourceProject {
class Data;
}

namespace Ui {
class ConfigureProjectView;
}

namespace ResourceProject
{
class View : public QWidget
{
    Q_OBJECT
    
public:
    explicit View(QWidget *parent = 0);
    ~View();

    static QString title() { return tr("Configure Project"); }
    static Global::IconLibrary::IconId icon() { return Global::Icons->Package; }
    
    void setData(ResourceProject::Data*);
    ResourceProject::Data* data() const { return m_data; }

    QString initViewData(QVariant) { return QString(); }

private slots:
    void on_btnSelResourcePath_clicked();
    void on_btnSelHeaderPath_clicked();

    void on_btnReload_clicked();

    void on_btnReset_clicked();

private:
    QDataWidgetMapper        *m_mapper;
    ResourceProject::Data    *m_data;
    Ui::ConfigureProjectView *ui;
};
} // namespace ResourceProject

#endif // VIEW_CONFIG_TABLE_H
