#ifndef USER_H
#define USER_H

#include "DataUtils.h"

class QXmlStreamWriter;
class QXmlStreamReader;

namespace User
{
class Data : public DataUtils::DataSource
{
    Q_OBJECT
    QString m_name;
    int     m_id;
public:
    Data(QObject *parent = 0);
    ~Data();

    void setName(QString s);
    const QString& name() const { return m_name; }

    void setId(int n);
    int id() const { return m_id; }

    void toXml(QXmlStreamWriter*);
    void fromXml(QXmlStreamReader*);

    enum PropID {
        Name,
        Id
    };
}; // Data
} // namespace User

#endif // USER_H
