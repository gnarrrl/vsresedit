#ifndef VIEW_USERDB_H
#define VIEW_USERDB_H

#include <QDialog>
#include <QModelIndex>

namespace Ui {
class ConfigureDialog;
}

class QSortFilterProxyModel;
class QDataWidgetMapper;

#include <QItemSelection>

namespace UserDb
{
class ConfigureDialog : public QDialog
{
    Q_OBJECT
    Ui::ConfigureDialog *ui;
    QSortFilterProxyModel *m_proxy;
    QDataWidgetMapper     *m_mapper;
public:
    explicit ConfigureDialog(QWidget *parent = 0);
    ~ConfigureDialog();

private slots:
    void on_btnNew_clicked();
    void on_btnDelete_clicked();
    void on_btnUp_clicked();
    void on_btnDown_clicked();
    void onSelectionChanged(QItemSelection,QItemSelection);

    QModelIndex selected();
};
}

#endif // VIEW_USERDB_H
