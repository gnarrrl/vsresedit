#include "ViewUserDb.h"
#include "ui_ViewUserdb.h"
#include "GlobalData.h"

#include <QSortFilterProxyModel>
#include <QDataWidgetMapper>
#include <QIntValidator>
#include <QSettings>

namespace UserDb
{
ConfigureDialog::ConfigureDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigureDialog)
{
    ui->setupUi(this);

    m_mapper = new QDataWidgetMapper(this);
    m_mapper->setModel(&Global::UserDb->Content);
    m_mapper->addMapping(ui->editName, Global::UserDb->Content.Name);
    m_mapper->addMapping(ui->editId, Global::UserDb->Content.Id);
    m_mapper->addMapping(ui->chkActiveUser, Global::UserDb->Content.Active);
    m_mapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    m_proxy = new QSortFilterProxyModel(this);
    m_proxy->setSourceModel(&Global::UserDb->Content);
    m_proxy->setDynamicSortFilter(true);
    ui->listView->setModel(m_proxy);
    ui->editId->setValidator(new QIntValidator(this));

    connect(ui->listView->selectionModel(),
            SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            SLOT(onSelectionChanged(QItemSelection,QItemSelection)));

    ui->listView->setDragDropMode(QAbstractItemView::InternalMove);
    ui->listView->setDragEnabled(true);
    ui->listView->setAcceptDrops(true);
    ui->listView->setDropIndicatorShown(true);

    QSettings settings;
    restoreGeometry(
                settings.value(QString("%1/geometry").arg(objectName())).toByteArray());
    ui->splitter->restoreState(
                settings.value(QString("%1/splitter").arg(objectName())).toByteArray());

    connect(ui->chkActiveUser, SIGNAL(clicked()), m_mapper, SLOT(submit()));
    connect(ui->editId, SIGNAL(textEdited(QString)), m_mapper, SLOT(submit()));
    connect(ui->editName, SIGNAL(textEdited(QString)), m_mapper, SLOT(submit()));
}

ConfigureDialog::~ConfigureDialog()
{
    QSettings settings;
    settings.setValue(QString("%1/geometry").arg(objectName()),
                      saveGeometry());
    settings.setValue(QString("%1/splitter").arg(objectName()),
                      ui->splitter->saveState());
    delete ui;
}

void ConfigureDialog::on_btnNew_clicked()
{
    Global::UserDb->createUser();
}

void ConfigureDialog::on_btnDelete_clicked()
{
    Global::UserDb->deleteUser(static_cast<User::Data*>(selected().internalPointer()));
}

QModelIndex ConfigureDialog::selected()
{
    QModelIndexList sel = ui->listView->selectionModel()->selectedIndexes();
    if (sel.size() == 0) return QModelIndex();
    return m_proxy->mapToSource(sel.at(0));
}

void ConfigureDialog::onSelectionChanged(QItemSelection newSel, QItemSelection)
{
    QModelIndexList sel = newSel.indexes();
    if (sel.size() == 1) {
        QModelIndex idx = m_proxy->mapToSource(sel.at(0));
        m_mapper->setCurrentModelIndex(idx);
    } else {
        ui->editId->clear();
        ui->editName->clear();
    }
}
} // namespace UserDb


void UserDb::ConfigureDialog::on_btnUp_clicked()
{
    QModelIndexList list = ui->listView->selectionModel()->selectedIndexes();
    if (list.size() == 0) return;
    QModelIndex sel = list.at(0);
    if (sel.row() > 0 && m_proxy->rowCount() > 1) {
        QModelIndex prevIndex = m_proxy->index(sel.row()-1, 0);
        if (prevIndex.isValid()) {
            User::Data *swap = static_cast<User::Data*>(m_proxy->mapToSource(sel).internalPointer());
            User::Data *with = static_cast<User::Data*>(m_proxy->mapToSource(prevIndex).internalPointer());
            Global::UserDb->swap(swap, with);
            ui->listView->selectionModel()->select(prevIndex, QItemSelectionModel::ClearAndSelect);
        }
    }
}

void UserDb::ConfigureDialog::on_btnDown_clicked()
{
    QModelIndexList list = ui->listView->selectionModel()->selectedIndexes();
    if (list.size() == 0) return;
    QModelIndex sel = list.at(0);
    if (sel.row() < m_proxy->rowCount()) {
        QModelIndex nextIndex = m_proxy->index(sel.row()+1, 0);
        if (nextIndex.isValid()) {
            User::Data *swap = static_cast<User::Data*>(m_proxy->mapToSource(sel).internalPointer());
            User::Data *with = static_cast<User::Data*>(m_proxy->mapToSource(nextIndex).internalPointer());
            Global::UserDb->swap(swap, with);
            ui->listView->selectionModel()->select(nextIndex, QItemSelectionModel::ClearAndSelect);
        }
    }
}
