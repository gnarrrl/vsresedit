#ifndef DataUtils_H
#define DataUtils_H

#include <QAbstractItemModel>
#include <QList>
#include <QPointer>

#include "ChangeNotify.h"

class QIODevice;

namespace DataUtils
{
class DataSource;

///////////////////////////////////////////////////////////////////////////////
// Model

class Model : public QAbstractItemModel
{
    Q_OBJECT
    QPointer<DataSource> m_data;
public:
    Model(DataSource *data, QObject *parent=0);

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    QModelIndex indexForObject(DataSource*, int column);

    template<typename T>
    T* dataSource() const { return qobject_cast<T*>(m_data.data()); }

    enum class StoreFormat {
        CSV
    };

    void store(StoreFormat, QIODevice*) const;

signals:
    void modified();

protected slots:
    virtual void onBeginInsertRows(int row, int count, DataSource *parent);
    virtual void onEndInsertRows();
    virtual void onBeginRemoveRows(int row, int count, DataSource *parent);
    virtual void onEndRemoveRows();
    virtual void onBeginReset(DataSource *source);
    virtual void onEndReset(DataSource *source);
    virtual void onDataChanged(int propertyId, DataSource *source);
    virtual void onBeginMoveRow(QPair<int, DataSource*> source,
                                QPair<int, DataSource*> destination);
    virtual void onEndMoveRow();

protected:
    virtual int rowInParent(DataSource *child) const { (void)child; return -1; }
    virtual DataSource* childInRow(DataSource *parent, int row) const = 0;
    virtual DataSource* parentOfChild(DataSource *child) const { (void)child; return 0; }    

protected:
    void privateDataChanged(int column, DataSource *source);
};

///////////////////////////////////////////////////////////////////////////////
// PropertyModel

class PropertyModel : public Model
{
    Q_OBJECT
public:
    PropertyModel(DataSource *data, QObject *parent=0)
        : Model(data, parent)
    {}

protected slots:
    virtual void onBeginInsertRows(int, int, DataSource*) override {}
    virtual void onEndInsertRows() override {}
    virtual void onBeginRemoveRows(int, int, DataSource*) override {}
    virtual void onEndRemoveRows() override {}
    virtual void onBeginReset(DataSource*) override {}
    virtual void onEndReset(DataSource*) override {}
    virtual void onBeginMoveRow(QPair<int, DataSource*>,
                                QPair<int, DataSource*>) override {}
    virtual void onEndMoveRow() override {}

protected:
    virtual int rowInParent(DataSource *child) const override;
    virtual DataSource* childInRow(DataSource *parent, int row) const override { (void)parent; (void)row; return 0; }
    virtual DataSource* parentOfChild(DataSource *child) const override { (void)child; return 0; }

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override { return parent.isValid() ? 0 : 1; }
    virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
};

///////////////////////////////////////////////////////////////////////////////
// DataSource

class DataSource : public QObject
{
    Q_OBJECT
public:
    DataSource(QObject *parent=0, ChangeNotifyManager *customManager=0);

    void connectDataSource(DataSource*);
    void connectChangeNotifier(ChangeNotifyManager *customManager=0);

    void enableUpdates(bool enable) { m_updatesEnabled = enable; }
    bool updatesEnabled() const { return m_updatesEnabled; }
    void setModified() { onModified(); }

    static ChangeNotifyManager StaticDataSourceChange;

signals:
    void beginReset(DataSource *source);
    void endReset(DataSource *source);
    void beginInsertRows(int row, int count, DataSource *parent);
    void endInsertRows();
    void beginRemoveRows(int row, int count, DataSource *parent);
    void endRemoveRows();
    void dataChanged(int propertyId, DataSource *source);
    void modified();
    void beginMoveRow(QPair<int, DataSource*> source,
                      QPair<int, DataSource*> target);
    void endMoveRow();

public slots:
    virtual void onBeginReset(DataSource* source);
    virtual void onEndReset(DataSource* source);
    virtual void onBeginInsertRows(int row, int count, DataSource *parent);
    virtual void onEndInsertRows();
    virtual void onBeginRemoveRows(int row, int count, DataSource *parent);
    virtual void onEndRemoveRows();
    virtual void onDataChanged(int propertyId, DataSource *source);
    virtual void onModified();
    virtual void onBeginMoveRow(QPair<int, DataSource*> source,
                                QPair<int, DataSource*> destination);
    virtual void onEndMoveRow();


private:
    bool m_updatesEnabled;
};

///////////////////////////////////////////////////////////////////////////////
// DataSourceProps

class DataSourceProps
{
public:
    DataSource* modelProperty(int dp)
    {
        Q_ASSERT(dp >= 0 && dp < m_modelProps.size());
        if (dp >= 0 && dp < m_modelProps.size()) {
            return m_modelProps.at(dp);
        } else {
            return 0;
        }
    }

    int indexOfModelProperty(DataSource *d)
    {
        return m_modelProps.indexOf(d);
    }

    int modelPropertyCount() const
    {
        return m_modelProps.size();
    }

protected:
    DataSourceProps() {}

    virtual ~DataSourceProps()
    {
        foreach (DataSource *d, m_modelProps) {
            delete d;
        }
    }

    void createDataSourceProps(DataSource *parent, int count)
    {
        for (int x=0; x<count; x++) {
            DataSource *d = new DataSource(parent);
            d->setProperty("propId", x);
            parent->connectDataSource(d);
            m_modelProps.push_back(d);
        }
    }

private:
    QList<DataSource*> m_modelProps;
};

} // namespace DataUtils

#endif // DataUtils_H
