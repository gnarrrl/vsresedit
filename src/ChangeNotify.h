#ifndef CHANGENOTIFY_H
#define CHANGENOTIFY_H

#include <QObject>

///////////////////////////////////////////////////////////////////////////////
// ChangeNotifyManager

class ChangeNotifyManager : public QObject
{
    Q_OBJECT
    bool m_documentModified;
public:
    friend class DataSource;
    ChangeNotifyManager();

    bool isDocumentModified() const { return m_documentModified; }

signals:
    void documentModified(bool);

public slots:
    void setDirty();
    void clearModified();

protected slots:
    void onDataChanged();
};

#endif // CHANGENOTIFY_H
