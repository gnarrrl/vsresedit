#include "Utils.h"
#include <QSortFilterProxyModel>

namespace Utils
{
QString makeUniqueObjectName(QObject *parent, QString base)
{
    QString objectName;
    int index = 1;
    do {
        objectName = base;
        if (index > 1) {
            objectName.append(QString("_%1").arg(index));
        }

        if (!parent->findChild<QObject*>(objectName)) {
            break;
        }
        index++;
    } while(1);
    return objectName;
}
}
