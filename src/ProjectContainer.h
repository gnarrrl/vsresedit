#ifndef PROJECT_CONTAINER_H
#define PROJECT_CONTAINER_H

#include "Project.h"
#include "DataUtils.h"
#include "ChangeNotify.h"

#include <QHash>
#include <QList>

namespace ProjectContainer
{
class Data;

extern ChangeNotifyManager* ChangeNotifier;

///////////////////////////////////////////////////////////////////////////////
// TreeModel

class ContentModel : public DataUtils::Model
{
    Q_OBJECT
public:
    ContentModel(Data*);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

protected slots:
    virtual void onBeginReset(DataUtils::DataSource *source);
    virtual void onEndReset(DataUtils::DataSource *source);

protected:
    virtual int rowInParent(DataUtils::DataSource *child) const;
    virtual DataUtils::DataSource* childInRow(DataUtils::DataSource *parent, int row) const;
    virtual DataUtils::DataSource* parentOfChild(DataUtils::DataSource *child) const;
}; // TreeModel

///////////////////////////////////////////////////////////////////////////////
// Data

class Data : public DataUtils::DataSource
{
    Q_OBJECT

    QPair<QHash<QString, ResourceProject::Data*>,
          QList<ResourceProject::Data*> > m_projects;
    QString m_configPath;

    QList<ResourceProject::Data*>& list() { return m_projects.second; }
    QHash<QString, ResourceProject::Data*>& map() { return m_projects.first; }

public:
    Data(QObject *parent=0);
    ~Data();

    ContentModel Content;

    void clear();

    const QList<ResourceProject::Data*>& projectList() const { return m_projects.second; }
    ResourceProject::Data* createProject();
    ResourceProject::Data* projectByGuid(QString guid);
    void                   deleteProject(QString guid);

    const QString& configPath() const { return m_configPath; }

signals:
    void configLoaded();

public slots:
    void loadFromSettings();
    void saveToSettings();

    void saveProjectsToDisk();

    void loadConfig(QString configPath);
    void saveConfig();

    void refreshProjects();
};
}

#endif // PROJECT_CONTAINER_H
