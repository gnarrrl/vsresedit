#ifndef ABSTRACTPROJECT_H
#define ABSTRACTPROJECT_H

#include <QAbstractItemModel>
#include <QHash>
#include <QList>
#include <QSet>

#include "StringTable.h"
#include "DataUtils.h"

class QXmlStreamReader;
class QXmlStreamWriter;
class QFileSystemWatcher;

namespace ResourceProject
{
class Data;

///////////////////////////////////////////////////////////////////////////////
// PropertyModel

class PropertyModel : public DataUtils::PropertyModel
{
    Q_OBJECT
public:
    PropertyModel(Data*);

    enum {
        ResourcePath,
        HeaderPath,
        _columnCount
    };

    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const { return parent.isValid() ? 0 : _columnCount; }
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
};

///////////////////////////////////////////////////////////////////////////////
// Data

class Data : public DataUtils::DataSource
{
    Q_OBJECT
    friend class PropertyModel;

    QString m_headerPath;
    QString m_resourcePath;
    QString m_id;
    QString m_name;
    QString m_description;
    QString m_tmpResourcePath;
    QString m_tmpHeaderPath;
    QSet<int> m_defines;
    QFileSystemWatcher *m_watcher;

    QPair<QHash<QString, StringTable::Data*>,
          QList<StringTable::Data*> > m_stringTables;

    QHash<QString, StringTable::Data*>& map() { return m_stringTables.first; }
    QList<StringTable::Data*>& list() { return m_stringTables.second; }

public:
    explicit Data(QObject *parent = 0);
    virtual ~Data();

    PropertyModel Properties;

    enum Property {
        Name
    };

    QString headerPath() const { return m_headerPath; }
    void setHeaderPath(QString s);

    QString resourcePath() const { return m_resourcePath; }
    void setResourcePath(QString s);

    QString id() const { return m_id; }

    QString name() const { return m_name; }
    void setName(QString s);

    QString description() const { return m_description; }
    void setDescription(QString s) { m_description = s; }

    const QHash<QString, StringTable::Data*>& stringTableMap() const { return m_stringTables.first; }
    const QList<StringTable::Data*>& stringTableList() const { return m_stringTables.second; }

    void sortedStringTables(QList<StringTable::Data*>*);

    StringTable::Data* stringTableById(QString scope);
    StringTable::Data* createStringTable(QString scope);
    void deleteStringTable(StringTable::Data*);

    int nextValue();

    void toXml(QXmlStreamWriter*);
    void fromXml(QXmlStreamReader*);

public slots:
    void clear();
    void loadFromDisk();
    void saveToDisk();
    void refreshChangesFromDisk();

    virtual void onBeginRemoveRows(int row, int count, DataSource *parent);

 signals:
    void nameChanged(QString);

protected slots:
    void onTableNameAboutToChange();
    void onTableNameChanged(QString);
    void onFileChanged(QString);

private slots:
    void privateLoadStrings();
    void privateLoadDefines();

    void privateSaveStrings();
    void privateSaveDefines();
};
}

#endif // ABSTRACTPROJECT_H
