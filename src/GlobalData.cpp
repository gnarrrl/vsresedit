
#include "GlobalData.h"

namespace Global
{
ProjectContainer::Data*  ResourceProjects;
IconLibrary*             Icons;
ActionsObject*           Actions;
IgnoreList::Data*        IgnoreList;
UserDb::Data*            UserDb;
QUndoStack*              UndoStack;
ChangeNotifyManager*     ChangeNotifier;
}
