#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDockWidget>
#include <QProxyStyle>
#include <QPainter>
#include <QStyleOption>
#include <QAction>
#include <QEvent>
#include "GlobalData.h"

class QTabWidget;

namespace Ui {
    class MainWindow;
}

class QAction;
class QDockWidget;

class IconProxyStyle : public QProxyStyle
{
    QIcon m_icon;

public:
    IconProxyStyle(const QIcon& icon,  QStyle* style = 0 )
        : QProxyStyle(style)
        , m_icon(icon)
    {}

    virtual void drawControl(ControlElement element, const QStyleOption* option,
                             QPainter* painter, const QWidget* widget = 0) const
    {
        /*
        if( element == QStyle::CE_DockWidgetTitle)
        {

            int width = pixelMetric(QStyle::PM_ToolBarIconSize);
            int margin = baseStyle()->pixelMetric( QStyle::PM_DockWidgetTitleMargin );

            QPoint icon_point( margin + option->rect.left(), margin + option->rect.center().y() - width/2 );
            const_cast<QStyleOption*>(option)->rect.adjust(0, 1, 0, 0);

            QLinearGradient shade(0, 0, 0, option->rect.height());

            QColor ltblue(240,240,240);
            shade.setColorAt(0, ltblue);
            shade.setColorAt(0.5, Qt::lightGray);
            shade.setColorAt(0.8, ltblue);
            shade.setColorAt(1, Qt::lightGray);
            shade.setSpread(QGradient::RepeatSpread);

            QPainterPath pp;
            pp.addRect(option->rect.left(), 10, option->rect.width(), option->rect.height()-10);
            pp.addRoundedRect(option->rect.left(), 0, option->rect.width(), option->rect.height(), 12, 8);
            pp.setFillRule(Qt::WindingFill);
            painter->setRenderHints(QPainter::Antialiasing);
            painter->fillPath(pp, shade);

            QRect textRect(option->rect);
            textRect.adjust(width+3, 0, 0, 0);
            painter->drawText(textRect, Qt::AlignVCenter, widget->windowTitle());
            painter->drawPixmap(icon_point, m_icon.pixmap( width, width ) );
        } else {*/
            baseStyle()->drawControl(element, option, painter, widget);
        //}
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    virtual void closeEvent(QCloseEvent *);

protected:
    bool eventFilter(QObject *obj, QEvent *event);

private:
    QString WindowTitleBase;

    void createCentralWidget();
    void createDockWindows();
    void connectActions();
    void createToolbar();
    bool maybeSave();

    template<class ViewType>
    QDockWidget* createDockWidget(QString objName, QVariant data)
    {
        if (findChild<QObject*>(objName)) return 0;

        QDockWidget *dock = new QDockWidget(ViewType::title(), this);
        dock->setObjectName(objName);

        ViewType *view = new ViewType(dock);
        QString customTitle = view->initViewData(data);
        dock->setWidget(view);
        dock->setFeatures(QDockWidget::DockWidgetMovable |
                          QDockWidget::DockWidgetFloatable |
                          QDockWidget::DockWidgetClosable);
        dock->setStyle(new IconProxyStyle(Global::Icons->get(ViewType::icon())));
        addDockWidget(m_dockWidgets.empty() ? Qt::LeftDockWidgetArea : Qt::RightDockWidgetArea, dock);

        dock->installEventFilter(this);
        if (!customTitle.isEmpty()) {
            dock->setWindowTitle(customTitle);
        }

        m_dockWidgets.push_back(dock);
        m_dockedViewMap.insert(dock, view);

        return dock;
    }

private slots:
    void onActionNewTree();
    void onActionConfigureTable();
    void onActionConfigureProject();
    void onActionConfigureIgnoreList();
    void onActionSelectConfigPath();
    void onActionConfigureUserList();
    void onActionShowUndoStack();

    void onDockClose(QDockWidget*);

    void onDocumentModified(bool);
    void onResourceConfigLoaded();
    void onAbout();

private:
    QList<QDockWidget*> m_dockWidgets;
    QHash<QDockWidget*, QWidget*> m_dockedViewMap;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
