#include "TutorialWizard.h"
#include "ui_TutorialWizard.h"
#include "GlobalData.h"

TutorialWizard::TutorialWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::TutorialWizard)
{
    ui->setupUi(this);

    connect(ui->btnSetConfigPath, SIGNAL(clicked()),
            Global::Actions->SetConfigPath, SLOT(trigger()));
    connect(ui->btnNewProject, SIGNAL(clicked()),
            Global::Actions->NewProject, SLOT(trigger()));
    connect(ui->btnCfgUsers, SIGNAL(clicked()),
            Global::Actions->ConfigureUserList, SLOT(trigger()));
    connect(ui->btnTipsNTricks, SIGNAL(clicked()),
            Global::Actions->TipsAndTricks, SLOT(trigger()));
}

TutorialWizard::~TutorialWizard()
{
    delete ui;
}
