
#include "IgnoreList.h"
#include "ModelTest.h"
#include "ChangeNotify.h"

#include <QDockWidget>
#include <QClipboard>
#include <QApplication>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

namespace IgnoreList
{
ChangeNotifyManager* ChangeNotifier;

///////////////////////////////////////////////////////////////////////////////
// Data

Data::Data() :
    Content(this)
{
    m_view = 0;
    //new ModelTest(&Content, this);
}

void Data::clear()
{
    emit beginReset(this);
    list().clear();
    map().clear();
    emit endReset(this);
}

bool Data::add(QString s)
{
    if (has(s)) return false;
    emit beginInsertRows(list().size(), 1, this);
    list().append(s);
    map().insert(s);
    emit endInsertRows();
    return true;
}

void Data::remove(QString s)
{
    if (!has(s)) return;
    int idx = list().indexOf(s);
    emit beginRemoveRows(idx, 1, this);
    list().removeAt(idx);
    map().remove(s);
    emit endRemoveRows();
}

bool Data::has(QString s)
{
    return (map().find(s) != map().end());
}

void Data::fromClipboard()
{
    emit beginReset(this);
    enableUpdates(false);
    QClipboard *clipboard = QApplication::clipboard();
    QString txt = clipboard->text();
    int pos = 0;
    int len = txt.length();
    QString pattern(": see previous definition of ");
    while(pos >= 0 && pos < len) {
        pos = txt.indexOf(pattern, pos);
        if (pos != -1) {
            int sqStart = txt.indexOf(QChar('\''), pos);
            int sqEnd = txt.indexOf(QChar('\''), sqStart+1);
            if (sqStart != -1 && sqStart != sqEnd) {
                QString id = txt.mid(sqStart+1, sqEnd-sqStart-1);
                add(id);
                pos = sqEnd+1;
            }
        }
    }
    enableUpdates(true);
    emit endReset(this);
}

void Data::setView(QDockWidget *view)
{
    if (m_view) return;
    m_view = view;
    if (m_view) {
        connect(m_view, SIGNAL(destroyed()), SLOT(onViewDestroyed()));
        connect(this, SIGNAL(destroyed()), m_view, SLOT(close()));
    }
}

void Data::change(QString oldText, QString newText)
{
    int idx = list().indexOf(oldText);
    if (idx == -1) return;
    QString &s = list()[idx];
    if (s == newText) return;
    map().remove(s);
    s = newText;
    map().insert(s);
    emit dataChanged(0, this);
}

void Data::toXml(QXmlStreamWriter *xs)
{
    foreach (const QString &s, list()) {
        xs->writeTextElement("entry", s);
    }
}

void Data::fromXml(QXmlStreamReader *xs)
{
    clear();
    while(!xs->atEnd()) {
        if (xs->readNextStartElement()) {
            QString name = xs->name().toString();
            if (name != "entry") break;
            add(xs->readElementText());
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
// ContentModel

ContentModel::ContentModel(Data *data) :
    DataUtils::Model(data)
{
}

DataUtils::DataSource* ContentModel::childInRow(DataUtils::DataSource *parent, int row) const
{
    (void)parent;
    QString *s = const_cast<QString*>(&dataSource<const Data>()->list().at(row));
    return reinterpret_cast<DataUtils::DataSource*>(s);
}

QVariant ContentModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::EditRole || role == Qt::DisplayRole) {
        QString *e = reinterpret_cast<QString*>(index.internalPointer());
        Q_ASSERT(e);
        if (e) {
            return *e;
        }
    }
    return QVariant();
}

bool ContentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (role == Qt::EditRole) {
        QString *e = reinterpret_cast<QString*>(index.internalPointer());
        Q_ASSERT(e);
        if (e) {
            dataSource<Data>()->change(*e, value.toString());
        }
        return true;
    }
    return false;
}

QVariant ContentModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    (void)section;
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        return tr("Ignore Define");
    }
    return QVariant();
}

int ContentModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    return dataSource<const Data>()->list().size();
}

Qt::ItemFlags ContentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) return Qt::NoItemFlags;
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}
}
