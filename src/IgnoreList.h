#ifndef IGNORELIST_H
#define IGNORELIST_H

#include <QSet>
#include <QStringList>

#include "DataUtils.h"
#include "ChangeNotify.h"

class QDockWidget;
class QXmlStreamReader;
class QXmlStreamWriter;

namespace IgnoreList
{
class Data;

extern ChangeNotifyManager* ChangeNotifier;

///////////////////////////////////////////////////////////////////////////////
// ContentModel

class ContentModel : public DataUtils::Model
{
    Q_OBJECT
public:
    ContentModel(Data*);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const
    {
        return parent.isValid() ? 0 : 1;
    }
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

protected:
    virtual DataUtils::DataSource* childInRow(DataUtils::DataSource *parent, int row) const;
};

///////////////////////////////////////////////////////////////////////////////
// Data

class Data : public DataUtils::DataSource
{
    Q_OBJECT

    QPair<QSet<QString>, QStringList> m_ignoreStrings;
    QDockWidget *m_view;

    QStringList& list() { return m_ignoreStrings.second; }
    QSet<QString>& map() { return m_ignoreStrings.first; }

public:
    Data();
    ContentModel Content;

    void clear();
    bool add(QString);
    void remove(QString);
    bool has(QString);
    void fromClipboard();
    void change(QString oldText, QString newText);
    const QStringList& list() const { return m_ignoreStrings.second; }

    QDockWidget* view() const { return m_view; }
    void setView(QDockWidget *view);

    void toXml(QXmlStreamWriter*);
    void fromXml(QXmlStreamReader*);

protected slots:
    void onViewDestroyed() { m_view = 0; }
};
}

#endif // IGNORELIST_H
