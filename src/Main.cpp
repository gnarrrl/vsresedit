#include <QApplication>
#include "MainWindow.h"

#include "Project.h"
#include "GlobalData.h"
#include "IconLibrary.h"
#include "ProjectContainer.h"
#include "ChangeNotify.h"

#include <QXmlStreamReader>
#include <QFile>
#include <QTime>
#include <QByteArray>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("Visual Studio Resource Editor");
    a.setOrganizationName("Dropping Coconut");

    Global::ResourceProjects = new ProjectContainer::Data;
    Global::Icons            = new Global::IconLibrary;
    Global::Actions          = new Global::ActionsObject;
    Global::IgnoreList       = new IgnoreList::Data;
    Global::UserDb           = new UserDb::Data;
    Global::UndoStack        = new QUndoStack;
    Global::ChangeNotifier   = new ChangeNotifyManager;

    IgnoreList::ChangeNotifier = new ChangeNotifyManager;
    UserDb::ChangeNotifier     = new ChangeNotifyManager;
    ProjectContainer::ChangeNotifier = new ChangeNotifyManager;

    Global::Icons->init();
    Global::Actions->init();
    Global::ResourceProjects->loadFromSettings();
    Global::UserDb->loadFromSettings();
    Global::IgnoreList->connectChangeNotifier(IgnoreList::ChangeNotifier);
    Global::UserDb->connectChangeNotifier(UserDb::ChangeNotifier);

    MainWindow w;
    w.show();

    int res = a.exec();

    delete Global::ResourceProjects;
    delete Global::Icons;
    delete Global::Actions;
    delete Global::IgnoreList;
    delete Global::UserDb;
    delete Global::UndoStack;
    delete Global::ChangeNotifier;

    delete IgnoreList::ChangeNotifier;
    delete UserDb::ChangeNotifier;
    delete ProjectContainer::ChangeNotifier;

    return res;
}
