#ifndef VIEW_CONFIG_TABLE_H
#define VIEW_CONFIG_TABLE_H

#include <QWidget>
#include <QModelIndex>
#include <QItemSelection>

#include "GlobalData.h"

class QSortFilterProxyModel;

namespace StringTable {
class Data;
}

namespace Ui {
class ConfigureTableView;
}

namespace StringTable
{
class View : public QWidget
{
    Q_OBJECT

public:
    explicit View(QWidget *parent = 0);
    ~View();

    static QString title() { return tr("Configure Table"); }
    static Global::IconLibrary::IconId icon() { return Global::Icons->Table; }

    void setData(StringTable::Data*);
    StringTable::Data* data() const { return m_data; }

    QString initViewData(QVariant data);

private slots:
    void on_actionDeleteSelected_triggered();
    void on_actionAddEntry_triggered();
    void on_editFilter_textChanged(const QString &arg1);
    void on_tableView_doubleClicked(const QModelIndex &index);

    void onDataDeleted() { m_data = 0; }
    void onTableSelectionChanged(QItemSelection,QItemSelection);

private:
    QString privatSettingsKey();

private:
    StringTable::Data *m_data;
    QSortFilterProxyModel *m_proxy;

    Ui::ConfigureTableView *ui;
};
}

#endif // VIEW_CONFIG_TABLE_H
