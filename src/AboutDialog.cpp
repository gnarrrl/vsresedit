#include "AboutDialog.h"
#include "ui_AboutDialog.h"
#include "GlobalData.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    ui->plainTextEdit->setPlainText(QString(tr("\nVersion %1 - Based on Qt %2 (32 bit)\n\n"
                                               "The program is provided AS IS with NO WARRANTY "
                                               "OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, "
                                               "MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE."))
                                            .arg(VSRE_VERSION_STR).arg(QT_VERSION_STR));

    ui->labelBuildTime->setText(QString(tr("Built on %1 %2")).arg(__DATE__).arg(__TIME__));
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
